// pages/choseLogin/choseLogin.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    logo: '',
    is_load: false,//是否请求接口
    loginType: 1,//1工程人员、2行政人员(默认工程人员)
    //backImg: app.globalData._network_path + 'chose-backimg.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // wx.setNavigationBarColor({
    //   frontColor: '#ffffff',
    //   backgroundColor: '#1cafff'
    // })

    this.getCompanyImg();//获取logo
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var loginType = wx.getStorageSync('loginType') == 2 ? 2 : 1;//登录类型，1工程人员、2行政人员(默认工程人员)
    app.loginStatus(function (res) {
      // that.getEngMsg();
      that.setData({
        login_status: true,
        is_load: true,
        loginType
      })
      if (loginType == 2) {
        setTimeout(() => {
          wx.reLaunch({
            url: '/pages/subpackage/pages/crm/index/index',
          })
        }, 1500);
      }else{
        setTimeout(() => {
          wx.switchTab({
            url: '../takeOrders/takeOrders/takeOrders',
          })
        }, 1500);
      }
    }, function (res) {
      that.setData({
        login_status: false,
        is_load: true,
        engineersInfo: '',
        loginType
      });
    })
  },

  // 获取logo
  getCompanyImg: function () {
    var that = this;
    var company_id = app.globalData.company_id
    // ajax请求
    app.ajax({
      url: 'Api/Company/getCompayConfig',
      url_type: 1,
      data: { company_id: company_id },
      success: function (res) {
        if (res.data.code == 1) {
          that.setData({
            logo: res.data.data.img_config.image
          })
        }
      }
    })
  },
  // 跳转登录
  loginFun: function (e) {
    var type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/pages/myCenter/login/login?type=' + type,
    })
  },
  //暂不登录>>
  notYetLogin(){
    wx.switchTab({
      url: '/pages/Allhome/Allhome',
    })
  }
})