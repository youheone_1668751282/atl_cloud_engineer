var util = require("../../../utils/util.js");
const app = getApp();
let time = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showCamera: true,
    device_no: '', //设备编号
    mainboard_no: '', //主板编号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.scanQrcodeCode();
  },
  onUnload(){
    clearTimeout(time);
  },

  scanQrcodeCode() {
    console.log('22222222222222')
    wx.scanCode({
      scanType:['barCode', 'qrCode', 'datamatrix'],
      success: res => {
        console.log('res',res);
        clearTimeout(time);
        const mainboardCode = res.result.indexOf('device_no=') <= -1 ?this.cutMainboardNum(res.result):'';
        const deviceCode = this.cutDeviceNum(res.result);
        console.log('mainboardCode',mainboardCode)
        console.log('deviceCode',deviceCode)

        let words = "";
        if (mainboardCode) {
          words = "主板码获取成功，请扫描设备码"
        }
        if (deviceCode) {
          words = "设备码获取成功，请扫描主板码"
        }
        if (mainboardCode != this.data.mainboard_no && mainboardCode) {
          wx.vibrateLong();
          this.setData({
            mainboard_no: mainboardCode
          })
        }
        if (this.device_no != this.data.device_no && deviceCode) {
          wx.vibrateLong();
          this.setData({
            device_no: deviceCode
          })
        }
        console.log('this.data.mainboard_no', this.data.mainboard_no);
        console.log('this.data.device_no', this.data.device_no);

        if (!this.data.device_no && !this.data.mainboard_no){
          wx.showToast({
            title: '二维码有误，请重新扫码',
            icon: "none",
            duration: 3000
          })

        } else if (!this.data.device_no || !this.data.mainboard_no){
            wx.showToast({
              title: words,
              icon: "none",
              duration: 3000
            })
          this.scanQrcodeCode();
        }else {

        }
        
      },
      fail:err=>{
        console.log('err',err)
      }
    })
  },
  //截取设备编号
  cutDeviceNum(str) {
    let result = "";
    if (str.indexOf("device_no=") > -1) {
      let strs = str.split("?device_no=");
      if (strs[1].indexOf("&") > -1) {
        result = strs[1].split("&")[0];
      } else {
        result = strs[1];
      }
    }
    return result;
  },
  //截取主板编号
  cutMainboardNum(str) {
    var that = this;
    let result = "";
    var code = str.split(';')
    if (!util.isExitsVariable(code) || code.length == 0) {
      app.showToast('扫码失败');
      return false;
    }
    var values = '';
    if (code[2] && code[2].indexOf(":") != -1) {
      var val = code[2].split(':');
      if (!util.isExitsVariable(val) || val.length == 0) {
        app.showToast('扫码失败');
        return false;
      }
      result = val[1];
    } else {
      result = code[0];
    } 
    console.log('进入', result);
    return result;
  },

  //绑定设备
  binding() {
    let mainboard_no = this.data.mainboard_no;
    let device_no = this.data.device_no;
    if (device_no == "") {
      app.showToast("请扫描设备编号");
      return;
    }
    if (mainboard_no == "") {
      app.showToast("请扫描主板号");
      return;
    }
    app.ajax({
      url: "Engineer/Equipmentlists/bind",
      data: {
        device_no: mainboard_no,
        qrcode_no: device_no,
      },
      success: res => {
        app.showToast(res.data.message);
        if (res.data.code == 1000) {
          this.setData({
            mainboard_no: '',
            device_no: ''
          })
        }
      }
    })
  },
})