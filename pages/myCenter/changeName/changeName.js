// pages/myCenter/changeName/changeName.js
var app=getApp();
var timer;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    token: wx.getStorageSync('token')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      name: options.name
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  // 输入昵称
  inputNameFun: function (e) {
    var name = e.detail.value;
    this.setData({
      name: name
    });
  },
  //修改名称
  editeNme() {
    var that = this;
    var name = that.data.name;
    if(name==''||name==null){
      app.showToast("昵称不能为空！","none",2000,function(){});
      return;
    }
    app.ajax({
      url: 'Engineer/Engineers/engineersEditInfo',
      method: 'POST',
      data: { engineers_name: name},
      header: { 'token': that.data.token},
      success: function(res){
        if(res.data.code == 1000){
          app.showToast("修改成功！2s后跳转","none",2000,function(){});
          timer = setTimeout(function(){
            wx.navigateBack({
              delta:1
            })
          },2000);
        }else{
          app.showToast("修改失败！请重试", "none", 2000, function () { });          
        }
      }
    })
    
  }
})