// pages/myCenter/econtractAudit/econtractAudit.js
var app = getApp();
let timer = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    subData: {},
    rotate: 0,
    isSuccess: 1,
    signImage:'',
    contract_id:'',
    user_id: '',
    notice:'',//错误信息
    work_order_id:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      signImage: options.signImage,
      contract_id: options.contract_id,
      user_id: options.user_id,
      work_order_id: options.work_order_id
    })
    that.fillPdf(options.signImage);
    timer = setInterval(function () {
      if (that.data.rotate == 0) {
        that.setData({
          rotate: 90
        })
      } else if (that.data.rotate == 90) {
        that.setData({
          rotate: 0
        })
      }
    }, 1000)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(timer);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  //生成PDF
  fillPdf: function (tempFilePath) {
    var that = this;
    wx.uploadFile({
      url: app.globalData._url + 'Common/Esignc/pdf',
      filePath: tempFilePath,
      name: 'file',
      header:{
        company: app.globalData.company_id,
        client: app.globalData.client,
      },
      formData: {
        'contract_id': that.data.contract_id
      },
      success: function (res) {
        var data = JSON.parse(res.data);
        if (data.code == 1000) {
          console.log(data.data.filename)
          that.signContract(data.data.filename, data.data.esign_user_sign);
        } else {
          that.setData({
            isSuccess: 3,
            notice: data.message
          })
        }
      },
      fail: function (err) {
        that.setData({
          isSuccess: 3,
          
        })
      }

    })
  },
  signContract: function (filename, esign_user_sign) {
    var that = this;
    app.ajax({
      url: 'Engineer/Contract/sign',
      data: {
        contract_id: that.data.contract_id,
        contact_type: 2,
        esign_filename: filename,
        esign_user_sign: esign_user_sign,
        sign_client: 2,
        work_order_id: that.data.work_order_id,
        user_id: that.data.user_id
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.code == 1000) {
          that.setData({
            isSuccess: 2
          })
        } else {
          that.setData({
            isSuccess: 3,
            notice:res.data.message
          })
        }
      },
      fail: function () {
        that.setData({
          isSuccess: 3
        })
      }
    })
  },
  // 重新签约
  again() {
    var that = this;
    wx.navigateBack({
      delta: 1,
    })
  },

  // 暂不签约
  nope() {
    var that = this;
    wx.navigateBack({
      delta: 2
    })
  }
})