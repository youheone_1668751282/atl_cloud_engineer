// pages/myCenter/serviceStation/serviceStation.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'', //服务站id
    stationData:[], //服务站信息
    token: wx.getStorageSync('token'),
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this;
    var id = options.id;
    that.setData({
      id: id
    })

  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getStationData();
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  //获取服务站信息
  getStationData: function(){
    var that =this;
    var id = that.data.id;
    app.ajax({
      url: 'Engineer/Engineers/getAdministrativeInfo',
      method: 'POST',
      data: { a_id: id},
      header: { 'token': that.data.token},
      success: function(res){
        if(res.data.code == 1000){
          that.setData({
            stationData: res.data.data
          })
        }else{
          app.showToast('暂无该服务站信息','none',2000,function(){});
          wx.navigateBack({
            delta:1
          })
        }
      }
    })
  },
  // 查看位置信息
  lookArea: function (e) {
    // var lat = parseFloat(e.currentTarget.dataset.lat);
    // var lng = parseFloat(e.currentTarget.dataset.lng);
    // var name = e.currentTarget.dataset.name;
    // var address = e.currentTarget.dataset.address;
    // console.log('lat', lat);
    // console.log('lng', lng);
    // return false;
    wx.getLocation({
      type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标  
      success: function (res) {
        // success  
        wx.openLocation({
          latitude: 29.72,//res.latitude, // 纬度，范围为-90~90，负数表示南纬  
          longitude: 106.62,//,res.longitude, // 经度，范围为-180~180，负数表示西经  
          scale: 15, // 缩放比例 5~18
          name: '不知道是哪里',
          address: '不知道是哪里不知道是哪里',
          success: function (res) {
          },
          fail: function () {

          }
        })
      }
    })
  }
})