// pages/user/unbind/unbind.js
const app = getApp();
var util = require("../../../utils/util.js");
let isRepeat = false;
Page({
	/**
   * 页面的初始数据
   */
	data: {
		deviceCode: '',//设备码
		mainboard: '',//主板码
	},

	/**
   * 生命周期函数--监听页面加载
   */
	onLoad: function(options) {
		isRepeat = false;
	},

	/**
   * 生命周期函数--监听页面初次渲染完成
   */
	onReady: function() {},

	/**
   * 生命周期函数--监听页面显示
   */
	onShow: function() {},

	/**
   * 生命周期函数--监听页面隐藏
   */
	onHide: function() {},

	/**
   * 生命周期函数--监听页面卸载
   */
	onUnload: function() {},

	/**
   * 页面相关事件处理函数--监听用户下拉动作
   */
	onPullDownRefresh: function() {},

	/**
   * 页面上拉触底事件的处理函数
   */
	onReachBottom: function() {},
	// 扫描设备主板码
	scanQrcodeCode() {
		wx.scanCode({
			scanType: [ 'barCode', 'qrCode', 'datamatrix' ],
			success: (res) => {
				console.log('res', res);
				let deviceCode = this.cutDeviceNum(res.result);
        let mainboard = res.result.indexOf('device_no=') <= -1 ? this.cutMainboardNum(res.result) : '';
				console.log('deviceCode', deviceCode);
				console.log('mainboard', mainboard);
				wx.vibrateLong();
				let word = '';
				if (!deviceCode && !mainboard) {
					word = '二维码有误，请重试';
				} else {
					word = '二维码扫描成功';
					this.setData({
						deviceCode: deviceCode,
						mainboard: mainboard
					});
				}
				wx.showToast({
					title: word,
					icon: 'none',
					duration: 3000
				});
			},
			fail: (err) => {
				console.log('err', err);
			}
		});
	},
	//截取设备编号
	cutDeviceNum(str) {
		let result = '';
    if (str.indexOf('device_no=') > -1) {
      let strs = str.split('?device_no=');
      result = strs[1];
		}
		return result;
	},
	//截取主板编号
	cutMainboardNum(str) {
    var that = this;
    let result = "";
    var code = str.split(';')
    if (!util.isExitsVariable(code) || code.length == 0) {
      app.showToast('扫码失败');
      return false;
    }
    var values = '';
    if (code[2] && code[2].indexOf(":") != -1) {
      var val = code[2].split(':');
      if (!util.isExitsVariable(val) || val.length == 0) {
        app.showToast('扫码失败');
        return false;
      }
      result = val[1];
    } else {
      result = code[0];
    }
    console.log('进入', result);
    return result;
	},
	// 解绑主板
	unbindMainboard() {
    if (!this.data.deviceCode && !this.data.mainboard) {
			app.showToast('请扫描二维码');
			return;
		}
		if (isRepeat) return;
		isRepeat = true;
		app.ajax({
      url: 'Engineer/Equipmentlists/unBind',
      data: { device_sn: this.data.deviceCode || this.data.mainboard },
			success: (res) => {
        app.showToast(res.data.message);
				if (res.data.code == 1000) {
					this.setData({
						deviceCode: '',
            mainboard: ''
					});
					isRepeat = false;
				} else {
					isRepeat = false;
				}
			}
		});
	}
});
