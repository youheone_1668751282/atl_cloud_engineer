// pages/versionList/versionList.js
var app = getApp();
let PAGE = 1;
let PAGESIZE = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//列表
    empty: '',//空数据
    hasMore: '',//是否还有更多
    loading: '',//加载更多
    is_waiting: '',//初始化加载
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      is_waiting: true
    })
    PAGE = 1;
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  // 获取列表
  getList() {
    let that = this;
    that.setData({
      loading: true
    })
    app.ajax({
      url: 'Common/Version/getVersionList',
      data: {
        'page': PAGE,
        'limit': PAGESIZE,
        // search: {
        //   client: 1
        // }
        'client': 5
      },
      success(res) {
        let list = this.data.list;
        let hasMore = true;
        let empty = false;
        if (res.data.code == 1000) {
          list = PAGE == 1 ? res.data.data : [...list, ...res.data.data];
          if (res.nowPage >= res.allPage) {
            hasMore = false;
          }
        } else {
          if (PAGE == 1) {
            list = [];
            empty = true;
          }
          hasMore = false;
        }

        that.setData({
          list: list,
          empty: empty,
          hasMore: hasMore,
          loading: false,
          is_waiting: false
        })
        
      }
    })
  },
  // 跳转详情
  goDetail(e) {
    wx.navigateTo({
      url: '../versionDetail/versionDetail?id=' + e.currentTarget.dataset.id + '&title=版本更新说明',
    })
  }
})