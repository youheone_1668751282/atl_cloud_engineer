// pages/subpackage/pages/crm/deviceReplace/deviceReplace.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    device_old: '',//旧设备id
    device_no: '',//旧设备号
    user_name: '',//用户姓名
    user_tel: '',//用户电话

    device_new: '',//新设备号
    product: '',//新设备下发产品id
    product_name: '',
    product_type: '',//新设备下发产品类型
    type: '',//1智能设备2非智能设备
    attr_value_id: '',//新设备选择的规格id
    suk: '',
    note: '',//备注

    equipmentsList: [],//产品列表
    attr_value_data: [],//规格
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      work_order_id: options.work_order_id
    });
    this.getProduct();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //选择设备
  navDeviceList() {
    var that=this;
    var work_order_id=that.data.work_order_id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/allDeviceList/allDeviceList?work_order_id=' + work_order_id,
    })
  },

  //获取产品
  getProduct(){
    var that = this;
    app.ajax({
      url: 'Engineer/Equipments/getList',
      data: {
        pageSize: 9999
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            equipmentsList: res.data.data.data
          })
        }
      }
    })
  },
  //选择下发的产品
  bindProduct(e) {
    var product_id = this.data.equipmentsList[e.detail.value].equipments_id;
    var product_name = this.data.equipmentsList[e.detail.value].equipments_name;
    var product_type = this.data.equipmentsList[e.detail.value].equipments_type;
    var type = this.data.equipmentsList[e.detail.value].type;
    this.setData({
      product: product_id,
      product_name: product_name,
      product_type: product_type,
      type: type,
      attr_value_data: [],
      attr_value_id: '',//新设备选择的规格id
      suk: '',
    })
    this.getSpecifications(product_id);
  },

  //获取规格
  getSpecifications(id) {
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/getProductSpecifications',
      data: {
        equipments_id: id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            attr_value_data: res.data.data
          })
        }
      }
    })
  },
  //选择规格
  bindPackage(e){
    var attr_value_id = this.data.attr_value_data[e.detail.value].attr_value_id;
    var suk = this.data.attr_value_data[e.detail.value].suk;
    this.setData({
      attr_value_id: attr_value_id,
      suk: suk,
    })
  },

  //新设备号输入
  devicenewInput(e){
    this.setData({
      device_new: e.detail.value
    })
  },
  //校验设备编号
  checkDevice(){
    var that = this;
    if (!that.data.device_new) {
      app.showToast('请输入新设备号');
      return
    }
    app.ajax({
      url: 'Engineer/Equipmentlists/checkDevice',
      data: {
        device_no: that.data.device_new
      },
      success: function (res) {
        app.showToast(res.data.message);
      }
    })
  },

  //提交的表单数据
  formSubmit(e) {
    console.log('提交的数据', e)
    var that = this;
    var value = e.detail.value;
    if (!that.data.device_old) {
      app.showToast('请选择旧设备');
      return
    } else if (!value.device_new && that.data.type != 2) {
      app.showToast('请输入新设备号');
      return
    } else if (!that.data.product) {
      app.showToast('请选择下发的产品');
      return
    } else if (!that.data.attr_value_id) {
      app.showToast('请选择规格');
      return
    }
    app.ajax({
      url: 'Engineer/Equipmentlists/doReplaceDevice',
      data: {
        device_one: that.data.device_old,
        device_two: value.device_new,
        product: that.data.product,
        attr_value_id: that.data.attr_value_id,
        note: value.remark
      },
      success: function (res) {
        app.showToast(res.data.message, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
             // wx.navigateBack();
              wx.redirectTo({
                url: '/pages/subpackage/pages/device/deviceDetail/deviceDetail?equipment_id=' + that.data.device_old,
              })
            }, 1000)
          }
        })
      }
    })
  },
  //表单重置
  formReset(){
    this.setData({
      device_old: '',//旧设备号
      user_name: '',//用户姓名
      user_tel: '',//用户电话
      device_new: '',//新设备号
      product: '',//新设备下发产品id
      product_name: '',
      package: '',//新设备下发套餐id
      package_name: '',
      note: '',//备注
    })
  },
})