// pages/shopping/realName/realName.js
var app = getApp();
var util = require("../../../utils/util.js");
let flag = false;//提交防抖
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeList: [],
    typeListI: 0, //证件类型  0 用户认证 1 企业认证 2政事认证
    name: '', //真实姓名
    idcard: '', //身份证号码
    legalperson: '', //法人代表
    licensenum: '', //统一社会信用代码
    comidcard: '',//企业身份证号码
    comname: '',//企业名称
    comaddress: '',//企业地址
    company_name:'',//单位名称
    company_address:'',//单位地址
    contact:'',//联系人
    tellphone:'',//联系电话
    isDisable: true, //按钮状态
    authInfo:{},//认证信息
    sub_loading:false,//提交按钮动画
    sub_txet:'下一步',//提交按钮文字说明
    readOnly:false,
    user_id:'',
    id_number_formate: '',
    userInfo:{},
    is_load: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if(util.isExitsVariable(options.user_id)){
      that.setData({
        user_id:options.user_id
      })
    }
    that.getAuthType();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    flag = false;
    that.getAuthInfo();
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },


  // 选择证件类型
  bindPickerChange(e) {
    this.setData({
      typeListI: e.detail.value
    })

    if (this.data.typeListI == 0) {
      if (this.data.name != '' && this.data.idcard != '') {
        this.setData({
          isDisable: false
        })
      } else {
        this.setData({
          isDisable: true
        })
      }
    }

    if (this.data.typeListI == 1) {
      if (this.data.legalperson != '' && this.data.licensenum != '' && this.data.comidcard != '' && this.data.comname != '' && this.data.comaddress != '') {
        this.setData({
          isDisable: false
        })
      } else {
        this.setData({
          isDisable: true
        })
      }
    }

    if (this.data.typeListI == 2) {
      if (this.data.company_name != '' && this.data.company_address != '') {
        this.setData({
          isDisable: false
        })
      } else {
        this.setData({
          isDisable: true
        })
      }
    }


  },

  // 绑定输入事件--用户名称
  getname(e) {
    this.setData({
      name: e.detail.value
    })
    if (this.data.typeListI == 0 && this.data.name != '' && this.data.idcard != '') {
      this.setData({
        isDisable: false
      })
    } else {
      this.setData({
        isDisable: true
      })
    }
  },
  // 绑定输入事件身份证号码
  getidcard(e) {
    this.setData({
      idcard: e.detail.value
    })
    if (this.data.typeListI == 0 && this.data.name != '' && this.data.idcard != '') {
      this.setData({
        isDisable: false
      })
    } else {
      this.setData({
        isDisable: true
      })
    }
  },


  //法人输入
  getlegalperson(e) {
    this.setData({
      legalperson: e.detail.value
    })
    this.exptionValues();
  },
  
  //统一社会信用代码
  getlicensenum(e) {
    this.setData({
      licensenum: e.detail.value
    })
    this.exptionValues();
  },

  //身份证
  getcomidcard(e) {
    this.setData({
      comidcard: e.detail.value
    })
    this.exptionValues();
  },
  //单位名称 
  getcomname(e) {
    this.setData({
      comname: e.detail.value
    })
    this.exptionValues();
  },
  //单位地址
  getcomaddress(e) {
    this.setData({
      comaddress: e.detail.value
    })
    this.exptionValues();
  },
  //12联系人
  getcontact(e) {
    this.setData({
      contact: e.detail.value
    })
    this.exptionValues();
  },

  //12联系方式
  gettellphone(e) {
    this.setData({
      tellphone: e.detail.value
    })
    this.exptionValues();
   
  },

  //验证是否可以提交（企业认证）
  exptionValues() {
    if (this.data.typeListI == 1 &&
      this.data.legalperson != '' &&
      this.data.licensenum != '' &&
      this.data.contact != '' &&
      this.data.tellphone != ''&&
      this.data.comidcard != '' &&
      this.data.comname != '' &&
      this.data.comaddress != '') {
      this.setData({
        isDisable: false
      })
    } else {
      this.setData({
        isDisable: true
      })
    }

  },


  getcontact22(e){
    this.setData({
      contact: e.detail.value
    })
    this.exptionValue();
  },
  gettellphone22(e){
    this.setData({
      tellphone: e.detail.value
    })
    this.exptionValue();
  },
  
  //单位名称
  getcompanyname(e) {
    this.setData({
      company_name: e.detail.value
    })
    this.exptionValue();
  },
  //单位地址
  getcompanyaddress(e) {
    this.setData({
      company_address: e.detail.value
    })
    this.exptionValue();
  },
  //统一社会信用代码
  getlicensenum(e) {
    this.setData({
      licensenum: e.detail.value
    })
    this.exptionValues();
  },
  
  //验证是否可以提交(政事认证)
  exptionValue(){
    if (this.data.typeListI == 2 &&
      this.data.company_name != '' &&
      this.data.company_address != '' &&
      this.data.contact != '' &&
      this.data.tellphone != '' &&
      this.data.licensenum != '') {
      this.setData({
        isDisable: false
      })
    } else {
      this.setData({
        isDisable: true
      })
    }
  },



  // 下一步
  subAuth(e) {
    var that = this;
    console.log(flag,"<<flag",that.data.isDisable,"<<<isDisable")
    if (flag) {
      return false;
    }
    if (that.data.isDisable) {
      return false;
    }
    that.setData({
      sub_txet:'正在验证中~~',
      sub_loading:true,
      isDisable:true
    })

    var typ_es = e.detail.value.types;
    var name = e.detail.value.name;
    var idcard = e.detail.value.idcard;
    var legalperson = e.detail.value.legalperson;
    var licensenum = e.detail.value.licensenum;
    var comidcard = e.detail.value.comidcard;
    var comname = e.detail.value.comname;
    var comaddress = e.detail.value.comaddress;
    var contact=e.detail.value.contact;
    var tellphone=e.detail.value.tellphone;
    var company_name=e.detail.value.company_name;
    var company_address=e.detail.value.company_address;

    // 用户认证
    if (typ_es == 0) {
      if (name == '') {
        app.showToast('请填写真实姓名');
        return false;
      }
      if (idcard == '') {
        app.showToast('请填写身份证号码');
        return false;
      }
      var datas = { 'id_number': idcard, "realname": name, 'user_id': that.data.user_id, 'auth_type': 1, "auth_client": 2 }
      
    };

    // 企业认证
    if (typ_es == 1) {
      if (legalperson == '') {
        app.showToast('请填写法人代表');
        return false;
      }
      if (licensenum == '') {
        app.showToast('请填写统一社会信用代码');
        return false;
      }
      if (comidcard == '') {
        app.showToast('请填写身份证号码');
        return false;
      }
      if (comname == '') {
        app.showToast('请填写单位名称');
        return false;
      }
      if (comaddress == '') {
        app.showToast('请填写单位地址');
        return false;
      }
      var datas = { 'license_number': licensenum, "legal_person": legalperson, 'comidcard': comidcard, 'comname': comname, 'comaddress': comaddress, "auth_type": 2, 'user_id': that.data.user_id, 'contact': contact, 'tellphone': tellphone, "auth_client": 2 };
     
    }

    //政事认证
    if (typ_es == 2) {
      if (company_name == '') {
        app.showToast('请填写单位名称');
        return false;
      }
      if (company_address == '') {
        app.showToast('请填写单位地址');
        return false;
      }
      var datas = { "auth_type": 3, 'user_id': that.data.user_id, 'company_name': company_name, 'company_address': company_address, 'contact': contact, 'tellphone': tellphone, "auth_client": 2, 'license_number': licensenum };
     
    }

    flag=true;
    // ajax请求
    app.ajax({
      url: 'Engineer/Realname/step1',
      data: datas,
      success: function (res) {
        that.setData({
          sub_txet: '下一步',
          sub_loading: false,
          //isDisable: false
        })
        //flag = false;
        if (res.data.code == 1000) {
          app.showToast("验证成功!正在跳转至下一步~~");
          setTimeout(function () {
            wx.navigateTo({
              url: '../uploadDocuments/uploadDocuments?types=' + typ_es+'&user_id=' + that.data.user_id
            })
          }, 2000)
        } else {
          that.setData({
            isDisable: false
          })
          flag = false;
          app.showToast(res.data.message);
        }


      }
    })

  },
  //获取用户认证信息
  getAuthInfo() {
    var that = this;
    app.ajax({
      url: "Engineer/Realname/getAuthInfo",
      data: {user_id:that.data.user_id},
      success: function (res) {
        if(res.data.code!=-1004){
          that.getUserInfo();
        }
        if (res.data.code == 1000) {
          var isDisable = true;
          if (util.isExitsVariable(res.data.data.realname) && util.isExitsVariable(res.data.data.id_number)){
            isDisable = false;
          }
          //是否可编辑
          var readOnly = false;
          if(res.data.data.status!=-1 && res.data.data.status!=3){
            readOnly = true;
          }
       
          that.setData({
            authInfo: res.data.data,
            name:res.data.data.realname,
            idcard: res.data.data.id_number,
            id_number_formate: res.data.data.id_number,
            isDisable: isDisable,
            readOnly: readOnly,
            typeListI: res.data.data.auth_type == 3 ? 2 : (res.data.data.auth_type ==2?1:0),
            legalperson: res.data.data.realname,
            licensenum: res.data.data.business_license_number,
            comidcard: res.data.data.id_number,
            comname: res.data.data.company_name,
            comaddress: res.data.data.company_address,
            contact:res.data.data.contact,
            tellphone:res.data.data.tellphone,
            company_name:res.data.data.company_name,
            company_address:res.data.data.company_address
          })
        }
        that.setData({
          is_load: true
        })
        
      }
    })
  },
  //获取用户信息
  getUserInfo: function () {
    var that = this;
    app.ajax({
      url: 'Engineer/User/getUserInfo',
      method: "POST",
      data: { user_id: that.data.user_id },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            userInfo: res.data.data
          })
        }
      }
    })
  },

  //获取认证类型
  getAuthType:function(){
    var that=this;
    app.ajax({
      url: 'Common/Common/getAuthType',
      method: "POST",
      data: { user_id: that.data.user_id },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            typeList: res.data.data
          })
        }
      }
    })

  }

})