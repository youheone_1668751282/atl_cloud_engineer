Component({
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定1111
    getSelect: {
      type: Number,
      observer: 'changeChoose'
    },
  },
  data: {
    selected: 0,
    color: "#BEC2CC",
    selectedColor: "#1cafff",
    list: [{
      pagePath: "/pages/subpackage/pages/crm/customerManag/customerManag",
      iconPath: "/pages/subpackage/images/crm/crm_user1.png",
      selectedIconPath: "/pages/subpackage/images/crm/crm_user2.png",
      text: "客户列表"
    }, {
        pagePath: "/pages/subpackage/pages/crm/certification/certification",
        iconPath: "/pages/subpackage/images/crm/crm_dispose1.png",
        selectedIconPath: "/pages/subpackage/images/crm/crm_dispose2.png",
      text: "实名认证"
    }, {
        pagePath: "/pages/subpackage/pages/crm/reportList/reportList",
        iconPath: "/pages/subpackage/images/crm/crm_remark1.png",
        selectedIconPath: "/pages/subpackage/images/crm/crm_remark2.png",
        text: "报备列表"
      }]
  },
  attached() {
  },
  methods: {
    //原来来的跳转是 switchTab,当做组建来使用我就换成redirectTo
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      if (data.index == this.data.selected){
          console.log('不跳转页面执行返回')
          return false
      }
      wx.redirectTo({url})
      this.setData({
        selected: data.index
      })
    },
    //监听每个页面传入的choose值
    changeChoose(e){
      var _vm=this;
      _vm.setData({
        selected: e
      });
    }
  }
})
