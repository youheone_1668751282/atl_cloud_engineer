// pages/user/econtractList/econtractList.js
var app = getApp();
var load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],//合同列表
    page: 1,//当前页
    pagesize: 10,//每页条数
    hasMore: false, //数据是否加载完成
    loading: false,
    load_show: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //var that = this;
    //that.getContractInfo();
  },



  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.setData({
      list: [],//合同列表
      load_show: false
    })
    that.getContractInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      list: [],//合同列表
      load_show: false,
      page: 1,
    })
    this.getContractInfo();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    var page = that.data.page + 1;
    that.setData({
      page: page,
      loading: true
    })
    that.getContractInfo();
  },
  //获取我的合同列表
  getContractInfo() {
    var that = this;
    var page = that.data.page;
    var pagesize = that.data.pagesize;
    app.ajax({
      url: 'Engineer/Renew/record',
      method: "POST",
      load: load,
      msg: '加载中...',
      data: {
        page: page,
        pagesize: pagesize
      },
      success: function (res) {
        //console.log(res)
        wx.stopPullDownRefresh()
        if (res.data.code == 1000) {
          //console.log(res.data.data);
          var list = that.data.list;
          res.data.data.forEach(function (v, k) {
            list.push(v);
          })
          that.setData({
            list: list,
          })
          load = false;
        } else {
          //app.showToast(res.data.message);
        }
        //是否需要加载更多
        var hasMore = true;
        if (res.data.data.length < pagesize) {
          hasMore = false;
        }
        that.setData({
          hasMore: hasMore,
          loading: false,
          load_show: true
        })
      }
    })
  },
  //查看合同详情
  lookEcontract: function (e) {
    //console.log(e);
    wx.navigateTo({
      url: '../econtract/econtract?contract_id=' + e.currentTarget.dataset.contract_id,
    })
  },
  //续费
  goRenew: function (e) {
    var contract_id = e.currentTarget.dataset.contract_id;
    wx.navigateTo({
      url: '../renew/renew?contract_id=' + contract_id,
    })
  }

})