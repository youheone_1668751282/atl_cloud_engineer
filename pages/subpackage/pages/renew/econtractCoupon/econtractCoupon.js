// pages/user/econtractCoupon/econtractCoupon.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    coupon0: '../../../images/coupon0.png',
    coupon1: '../../../images/coupon1.png',
    coupon2: '../../../images/coupon2.png',
    list: [],
    coupon:[],//选中的优惠券
    hasMore: false, //数据是否加载完成
    loading: false,
    load_show: false,
    user_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('options',options)
    var that = this;
    if (options.coupon){
      that.setData({
        coupon: options.coupon.split('#')
      })
    }
    if (options.user_id) {
      that.setData({
        user_id: options.user_id
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    /*wx.getStorage({
      key: 'coupon',
      success(res) {
        console.log('coupon', res.data)
        that.setData({
          coupon: res.data
        })
      }
    })*/
    _PAGE = 1;
    that.getCouponList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    _PAGE = 1;
    that.getCouponList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    _PAGE++;
    that.getCouponList(); // 获取优惠券列表
  },

  // 获取我的优惠券列表
  getCouponList() {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'Engineer/Renew/getCoupon',
      data: {
        page: _PAGE,
        row: _PAGESIZE,
        user_id:that.data.user_id
      },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          var dataList = [];
          if (_PAGE == 1) {
            dataList = res.data.data.data;
            // that.setData({
            //   list: res.data.data.data,
            // });
          } else {
            dataList = that.data.list.concat(res.data.data.data);
            // that.setData({
            //   list: that.data.list.concat(res.data.data.data),
            // });
          }

          // var dataList = that.data.list;
          if (that.data.coupon.length > 0) {
            that.data.coupon.map((val) => {
              dataList.map((value) => {
                if (value.coupon_id == val) {
                  value.checkbox = true
                }
              })
            })
          }

          var hasMore = true;
          if (res.data.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            list: dataList,
            hasMore: hasMore,
            loading: false
          });

        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
        that.setData({
          load_show: true
        })
      }
    })
  },
  //选择
  chooseCoupon(e){
    var that = this;
    if (e.currentTarget.dataset.status==1){
      let selArr = that.data.coupon;
      let selId = e.currentTarget.dataset.id;
      let dataList = that.data.list;
      let index = that.data.coupon.indexOf(selId);
      if (index < 0) {
        if (that.data.coupon.length >= 12) {
          app.showToast('不能超过12个');
          return
        } else {
          selArr.push(selId);
          dataList.map((value) => {
            if (value.coupon_id == selId) {
              value.checkbox = true
            }
          })
        }
      } else {
        dataList.map((value) => {
          if (value.coupon_id == selId) {
            value.checkbox = false
          }
        })
        selArr.splice(index, 1)
      }

      that.setData({
        coupon: selArr,
        list: dataList
      })
    }else{
      app.showToast('该优惠券不可用');
      return false;
    }
  },
  //确认选择
  sure(){
    var that = this;
    console.log("coupon返回", that.data.coupon)
    // wx.setStorageSync('coupon', that.data.coupon);
    // 将参数传回上一页
    const pages = getCurrentPages()
    const prevPage = pages[pages.length - 2] // 上一页
    // 调用上一个页面的setData 方法，将数据存储
    prevPage.setData({
      coupon: that.data.coupon
    })
    // 返回上一页
    wx.navigateBack({
      delta: 1
    })
  },

})