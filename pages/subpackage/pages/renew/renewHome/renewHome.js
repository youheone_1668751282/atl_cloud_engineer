// pages/subpackage/pages/renewHome/renewHome.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    keyword:'',//关键字
    lists:[],
    is_search:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      is_search:false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  // 续费
  goRenew(e){
    var contract_id = e.currentTarget.dataset.contract_id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/renew/renew/renew?contract_id=' + contract_id,
    })
  },
  //输入框监听
  inputSearch(e){
    var keyword = e.detail.value;
    var that = this;
    that.setData({
      keyword:keyword
    })
  },
  //搜索
  searchOrder(){
    var that = this;
    var keyword = that.data.keyword;
    var URL = 'Engineer/Renew/searchContractByUser';
    app.ajax({
      url: URL,
      method: "POST",
      load: true,
      msg: '搜索中...',
      data: {
        keyword: keyword
      },
      success: function (res) {
        var data = [];
       if(res.data.code==1000){
         data = res.data.data;
       }
        that.setData({
          lists:data,
          is_search:true
        })
      }
    });
  },
  //续费记录
  record(){
    wx.navigateTo({
      url: '../renewRecord/renewRecord',
    })
  }
})