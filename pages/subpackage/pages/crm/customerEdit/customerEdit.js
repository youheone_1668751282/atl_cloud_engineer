// pages/subpackage//pages/crm/customerEdit/customerEdit.js
//editType: 1, //1查看  2新增  3编辑

var util = require("../../../../../utils/util.js");
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_id: '',//客户id
    source_id: '',//推荐人id
    source_name: '',//推荐人name
    source_tel: '',//推荐人电话

    operationList: [],//运营中心列表
    sexList: [{ name: '男', value: 1, checked: 'true'}, { name: '女', value: 2}],//性别
    old_client: [{ name: '是', value: 1 }, { name: '否', value: 0, checked: 'true'} ],//是否是老系统,0否1是
    account_status: [{ name: '正常', value: 1, checked:'true'}, { name: '冻结', value: 2 }],//账号状态,1正常2冻结

    valueName: [],//设置回显选择的地址(name)
    getAreaMsg: '',//选择的地址信息
    cusDetail:{
      username:'',
      realname:'',
      id_card:'',
      emergency_contact_name:'',
      gender:'',
    },//获取到得用用户信息
    belongToAreas: [
      {
        o_id: '',
        a_id: '',
        a_name: "",
        o_name: "",
        administrativeList: []
      }
    ],//所属管理区域

    editType: 1, //1查看  2新增  3编辑
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //showtype 对应的值1查看  2新增  3编辑
    var that = this;
    if (util.isExitsVariable(options.showtype)) {
      that.setData({
        editType: options.showtype,
        user_id: options.user_id
      })
      if (options.showtype == 1){
        this.getCustomerDetail(options.user_id);
      } else if (options.showtype == 2){

      } else if (options.showtype == 3){
        this.getCustomerDetail(options.user_id);
      }
    }
    that.headid = this.selectComponent("#addressid"); //引入地址
    that.getBelongToAreas(options.user_id);//获取所属区域
    that.getOperationList();//获取运营中心列表
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  //选择性别
  radioGender: function (e) {
    var sexList = this.data.sexList;
    sexList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      sexList
    })
  },
  //选择是否是老系统
  radioOldClient(e){
    var old_client = this.data.old_client;
    old_client.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      old_client
    })
  },
  //状态系统
  radioAccountStatus(e){
    var account_status = this.data.account_status;
    account_status.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      account_status
    })
  },
  //打开地图选择
  openAddress() {
    var that = this;
    if (that.data.editType==1){
      return false
    }
    this.headid.startAddressAnimation(true);
  },
  //确定接受
  getSure(data) {
    console.log('确定事件获取地址', data);
    var that = this;
    let [province_code, city_code, area_code] = data.detail.areaId;
    let [province, city, area] = data.detail.areaName;
    that.setData({
      getAreaMsg: data.detail
    })
  },
  //提交的表单数据
  formSubmit(e) {
    console.log('提交的数据', e)
    var that = this;
    var val = e.detail.value;
    var reg = /^[\u4E00-\u9FA5A-Za-z]+$/; 
    console.log(val.realname, reg.test(val.realname))
    if (val.username == '') {
      app.showToast('请输入称呼');
      return false;
    }
    if (val.realname==''){
      app.showToast('请输入真实姓名');
      return false;
    }
    if (!reg.test(val.realname)){
      app.showToast('真实姓名只能是汉字');
      return false;
    }
    var operation_id = [];
    var a_id = [];
    that.data.belongToAreas.forEach((item)=>{
      if (item.o_id && !item.a_id) {
        app.showToast('请选择城市合伙人');
        return false
      }
      if (item.o_id && item.a_id) {
        operation_id.push(item.o_id);
        a_id.push(item.a_id);
      }
    })
    app.ajax({
      url: that.data.editType == 3 ? 'api/Customer/edit' :'api/Customer/add',
      url_type: 2,
      data: {
        user_id: that.data.user_id,
        username: val.username,
        realname: val.realname,
        telphone: val.telphone,
        id_card: val.id_card,
        emergency_contact_name: val.emergency_contact_name,
        emergency_contact_tel: val.emergency_contact_tel,
        operation_id: operation_id,
        a_id: a_id,
        gender: val.gender,
        province_code: that.data.getAreaMsg ? that.data.getAreaMsg.areaId[0] : '',
        city_code: that.data.getAreaMsg ? that.data.getAreaMsg.areaId[1] : '',
        area_code: that.data.getAreaMsg ? that.data.getAreaMsg.areaId[2] : '',
        user_address: val.user_address,
        old_client_name: that.data.old_client[0].checked ? val.old_client_name : '',
        old_client_tel: that.data.old_client[0].checked ? val.old_client_tel : '',
        source: that.data.source_id,
        is_old_client: val.oldMater,
        account_status: val.accounttype,
        remark: val.remark,
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        })
      }
    })
  },
  //获取用户信息详情
  getCustomerDetail(id){
    var that = this;
    app.ajax({
      url: 'api/Customer/getCustomerDetail',
      url_type: 2,
      data: {
        user_id:id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          // 处理性别回显 sexList
          const sexList = that.data.sexList;
          sexList.forEach(function (item, index) {
            if (item.value == res.data.data.gender) {
              item.checked = true;
            } else {
              item.checked = false;
            }
          });
          // 老系统客户介绍 old_client
          const old_client = that.data.old_client;
          old_client.forEach(function (item, index) {
            if (item.value == res.data.data.is_old_client) {
              item.checked = true;
            } else {
              item.checked = false;
            }
          });
          // 处理性别回显 sexList
          const account_status = that.data.account_status;
          account_status.forEach(function (item, index) {
            if (item.value == res.data.data.account_status) {
              item.checked = true;
            } else {
              item.checked = false;
            }
          });

          

          that.setData({
            cusDetail: res.data.data,
            source_id: res.data.data.source,
            source_name: res.data.data.source_name,
            source_tel: res.data.data.source_tel,
            sexList,
            old_client,
            account_status,
            getAreaMsg: res.data.data.province?{
              areaName: [res.data.data.province, res.data.data.city, res.data.data.area],
              areaId: [res.data.data.province_code, res.data.data.city_code, res.data.data.area_code]
            }:'',
            valueName: [res.data.data.province, res.data.data.city, res.data.data.area],
          })
        }
      }
    })
  },
  //获取所属区域;
  getBelongToAreas(id){
    var that = this;
    app.ajax({
      url: 'api/Customer/getBelongToAreas',
      url_type: 2,
      data: {
        user_id: id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          res.data.data.forEach((item)=>{
            that.getAdministrative(item.o_id);
          })
          
          that.setData({
            belongToAreas: res.data.data
          })
        }
      }
    })
  },
  //获取运营中心
  getOperationList() {
    var that = this;
    app.ajax({
      url: 'api/Operation/getList',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            operationList: res.data.data
          })
        }
      }
    })
  },
  //获取运营中心
  getAdministrative(id) {
    var that = this;
    app.ajax({
      url: 'api/Administrative/getAdministrative',
      url_type: 2,
      data: {
        operation_id: id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var temp = that.data.belongToAreas;
          temp.forEach((item,index)=>{
            if(item.o_id == id){
              item.o_index = index;
              item.administrativeList = res.data.data;
            }
            res.data.data.forEach((it_em,ind_ex)=>{
              if(item.a_id == it_em.a_id){
                item.a_index = ind_ex;
              }
            })
          })
          that.setData({
            belongToAreas: temp
          })
        }
      }
    })
  },
  //添加管理区域
  addArea(){
    var temp = this.data.belongToAreas;
    temp.push({
      o_id: '', 
      a_id: '', 
      a_name: "", 
      o_name: "", 
      administrativeList: []
    })
    this.setData({
      belongToAreas: temp
    })
  },
  //删除管理区域
  deleteArea(e){
    var index = e.currentTarget.dataset.index;
    var temp = this.data.belongToAreas;
    temp.splice(index,1);
    this.setData({
      belongToAreas: temp
    })
  },
  //运营中心picker
  bindAreaO(e){
    var temp = this.data.belongToAreas;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    temp[index].o_index = value;
    temp[index].o_id = this.data.operationList[value].o_id;
    temp[index].o_name = this.data.operationList[value].company_name;
    this.setData({
      belongToAreas: temp
    })
    this.getAdministrative(this.data.operationList[value].o_id);
  },
  //合伙人picker
  bindAreaA(e){
    var temp = this.data.belongToAreas;
    var value = e.detail.value;
    var index = e.currentTarget.dataset.index;
    temp[index].a_index = value;
    temp[index].a_id = temp[index].administrativeList[value].a_id;
    temp[index].a_name = temp[index].administrativeList[value].company_name;
    this.setData({
      belongToAreas: temp
    })
  },
  //选择推荐人
  navCustomerList(){
    wx.navigateTo({
      url: '../exchange/exchange?way=3',
    })
  }
})