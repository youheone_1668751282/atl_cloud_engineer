// pages/subpackage//pages/crm/noticeList/noticeList.js
var app = getApp();
var pagea = 1;
var pageSize = 20;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noticesList: [],
    is_load: false,
    hasMore: false,
    loading:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    pagea = 1;
    this.setData({
      noticesList: [],
      is_load: false,
      hasMore: true,
    })
    this.getnoticelists();//获取列表
  },
  /*** 页面上拉加载*/
  onReachBottom: function () {
    var that = this;
    pagea += 1;
    // 当没有数据时,不再请求
    if (!that.data.hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    that.getnoticelists(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      noticesList: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getnoticelists(); // 获取订单列表
  },
  // 获取列表
  getnoticelists: function () {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/announcement/getlists',
      url_type: 2,
      data: {
        page: pagea,
        pageSize: pageSize,
        key:'',
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.noticesList;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            newlist = oldlist.concat(res.data.data);
          }
          if (res.data.count <= newlist.length) {
            that.setData({
              hasMore: false
            })
          }
          console.log('最后的数据', newlist)
          newlist.forEach(function(item,index){
            item.time = item.create_time.substr(0, 10)
          })

          that.setData({
            noticesList: newlist
          })
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false,
          popShow: false,
        })

        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },

  // 跳转详情
  goDetail(e) {
    var index = e.currentTarget.dataset.index;
    var title = this.data.noticesList[index].title;
    var content = JSON.stringify(this.data.noticesList[index].content);
    wx.navigateTo({
      url: '../noticeDetail/noticeDetail?title=' + title + '&content=' + encodeURIComponent(content),
    })
  }
})