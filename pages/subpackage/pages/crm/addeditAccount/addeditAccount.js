// pages/subpackage/pages/crm/addeditAccount/addeditAccount.js
var app = getApp();
var isRepeat = false;
var WxParse = require('../../../wxParse/wxParse.js');
let accountTypeWay = false, bank_code = '';//账号类型是否为json数据,开户行

 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: 1,
    accountTypeKey:[],//账号类型key数组
    accountType: ['银行卡'],//账号类型数组
    accountTypeI: 0,//选择的账号类型索引index
    accountTypeSelect: 0,//选择的账号类型key
    username: '',
    openingBank: [],
    openingBankI: 0,
    bankcard: '',
    phonenum: '',
    idcard: '',
    openingaddress: '',
    agree: false,
    agreeShow: false,
    bank_name: '',
    confirmswitch: false,
    bank_id: 0,
    openbank: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;   
    if (options.type == 1) {
      // 编辑
      wx.setNavigationBarTitle({
        title: '编辑银行卡'
      })
      that.setData({
        bank_id: options.bank_id,
        agree:true,
        type: 1
      })
    } else if (options.type == 2) {
      // 添加
      wx.setNavigationBarTitle({
        title: '添加银行卡'
      })
      that.setData({
        type: 2
      })
    }
    that.getBalanceConfig();
    that.getBankList();
    that.getArticle();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    isRepeat = false;

  },

 
  // 获取结算配置
  getBalanceConfig() {
    var that = this;
    app.ajax({
      url: 'Common/Common/getBalanceConfig',
      method: 'POST',
      data: {
        key: 'cash_account_type'
      },
      success(res) {
        if(res.data.code == 1000){
          var value = res.data.data.value;
          // 当账号类型为json数据时
          if (typeof value === 'object' && isNaN(value.length)){
            var accountType =[];
            var accountTypeKey = [];
            for(var i in value){
              accountType.push(value[i]);
              accountTypeKey.push(i);
            }
            that.setData({
              accountType: accountType,
              accountTypeKey: accountTypeKey
            })
            accountTypeWay = true;

          }else{
            // 当账号类型为普通数组时
            accountTypeWay = false;
            that.setData({
              accountType: res.data.data.value
            })
          }

          
        }
      }

    })
  },
  // 选择账号类型
  accountTypeChange: function(e) {
    var that = this;
    var accountTypeI = e.detail.value;
    var accountTypeKey = that.data.accountTypeKey;
    that.setData({
      accountTypeI: accountTypeI
    })
    if (accountTypeWay){
      that.setData({
        accountTypeSelect: accountTypeKey[accountTypeI]
      })
    }else{
      that.setData({
        accountTypeSelect: accountTypeI
      })
    }
    
  },

  // 选择开户行
  openingBankChange: function(e) {
    var that = this;
    var i = e.detail.value
    bank_code = that.data.openingBank[i].bank_code;
    var name = that.data.openingBank[i].bank_name;
    that.setData({
      openingBankI: i,
      bank_name: name
    })
  },

  // 服务协议
  agreement: function() {
    this.setData({
      agreeShow: true
    })
  },

  //同意协议
  selectagree: function() {
    this.setData({
      agree: !this.data.agree
    })
  },

  // 同意服务条款
  agreeclause: function() {
    this.setData({
      agreeShow: false,
      agree: true
    })
  },

  // 确认
  formSubmit: function(e) {
    var that = this;
    var bank_id = that.data.bank_id;
    // var bank_type = parseInt(e.detail.value.accountType) + 1
    var bank_type = parseInt(that.data.accountTypeSelect)+1;
    var bankcard = e.detail.value.bankcard;
    var bank_name = that.data.bank_name;
    var name = e.detail.value.username;
    var id_card = e.detail.value.idcard;
    var bank_phone = e.detail.value.phonenum;
    var address = e.detail.value.openingaddress;
    console.log();
    if (that.data.agree == false) {
      app.showToast('请同意服务协议')
      return false;
    }
    console.log('isRepeat', isRepeat);
    if (isRepeat) {
      return;
    }
    isRepeat = true;
    if(that.data.type == 1){
      app.ajax({
        url: "api/Withdraw/editBank",
        url_type:2,
        data: {
          bank_id: bank_id,
          bank_type: bank_type,
          bank_number: bankcard,
          bank_name: bank_name,
          bank_code: bank_code,
          name: name,
          id_card: id_card,
          bank_phone: bank_phone,
          address: address
        },
        success: function (res) {
          isRepeat = false;
          if (res.data.code == 1000) {
            app.showToast(res.data.msg)
            that.setData({
              confirmswitch: true
            })
            setTimeout(function () {
              wx.navigateBack({
                delta: 1,
              })
            }, 500)
          } else {
            app.showToast(res.data.msg)
          }
        }
      })
    }else{
      app.ajax({
        url: "api/Withdraw/addBank",
        url_type:2,
        data: {
          bank_type: bank_type,
          bank_number: bankcard,
          bank_name: bank_name,
          bank_code: bank_code,
          name: name,
          id_card: id_card,
          bank_phone: bank_phone,
          address: address
        },
        success: function (res) {
          isRepeat = false;
          if (res.data.code == 1000) {
            app.showToast(res.data.msg)
            that.setData({
              confirmswitch: true
            })
            setTimeout(function () {
              wx.navigateBack({
                delta: 1,
              })
            }, 500)
          } else {
            app.showToast(res.data.msg)
          }
        }
      })
    }  
  },

  // 获取银行卡列表
  getBankList() {
    var that = this;
    app.ajax({
      url: "api/Customer/getSearchConfig",
      url_type:2,
      data: {config_name:'bank_type'},
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            openingBank: res.data.data,
            bank_name: res.data.data[0].bank_name,
          })
          bank_code = res.data.data[0].bank_code;
        }
        if(that.data.type == 1){
          that.getBankInfo()
        }       
      }
    })
  },

  // 获取银行卡详情
  getBankInfo() {
    var that = this;
    app.ajax({
      url: "api/Withdraw/getBankInfo",
      url_type:2,
      data: {
        bank_id: that.data.bank_id
      },
      success: function(res) {
        if (res.data.code == 1000) {
          var bank_code = res.data.data.bank_code;
          var k = 0;
          console.log(that.data.openingBank)
          var bank_type = parseInt(res.data.data.bank_type) - 1;
          for (var i = 0; i < that.data.openingBank.length; i++) {
            console.log(bank_code)
            if (that.data.openingBank[i].bank_code == bank_code) {
              console.log(that.data.openingBank[i].bank_code)   
              k = i 
              break           
            }            
          }
          var accountTypeKey = that.data.accountTypeKey;
          var accountTypeI = bank_type
          if (accountTypeWay){
            for (var i in accountTypeKey){
              if (bank_type == accountTypeKey[i]){
                accountTypeI = i;
                break
              }
            }
          }

          that.setData({
            accountTypeI: accountTypeI,
            accountTypeSelect: bank_type,
            username: res.data.data.name,
            openingBankI: k,
            bankcard: res.data.data.bank_number,
            phonenum: res.data.data.bank_phone,
            idcard: res.data.data.id_card,
            openingaddress: res.data.data.address,
          })
        }
      }
    })
  },
  // 获取服务协议
  getArticle: function () {
    var that = this;
    // Ajax请求
    app.ajax({
      url: 'api/Article/getList',
      url_type:2,
      data: { cate_sn: "Ad6nJUDF" },
      success: function (res) {
        if (res.data.code == 1000) {
          var content = res.data.data[0].content;
          WxParse.wxParse('article', 'html', content, that, 5);
        }

      }
    })
  },
  
})