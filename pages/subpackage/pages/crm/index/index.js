// pages/crm/loading/loading.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: '',
    noticesList: {},
  },

  /*** 生命周期函数--监听页面加载*/
  onLoad: function (options) {
    this.getInfo();
    this.getnoticelists();
  },
  onShow: function () {
    this.hidebuttonFun();
  },

  //现获取基础库版本,并隐藏homebutton
  hidebuttonFun() {
    wx.getSystemInfo({
      success: function (res) {
        var version = res.SDKVersion;
        version = version.replace(/\./g, "")
        console.log('结果', version)
        if (parseInt(version) >= 283) {
          wx.hideHomeButton();
        }
      }
    })
  },
  //获取公告
  getnoticelists: function () {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/announcement/getlists',
      url_type: 2,
      data: {
        page: 1,
        pageSize: 1,
        key: '',
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        if (res.data.code == 1000) {
          that.setData({
            noticesList: res.data.data[0],
          })
        } else {
        }

      }
    })
  },
  //跳转 公告
  noticeList() {
    wx.navigateTo({
      url: '../noticeList/noticeList',
    })
  },
  
  //获取用户信息详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'api/Users/getLoginUsers',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            info: res.data.data,
          })
        }
      }
    })
  },
  //跳转控制台
  goConsole() {
    var that = this;
    var client = that.data.info.client;
    wx.navigateTo({
      url: '../chart/chart?client=' + client,
    })
  },
  //退出登录
  logout(){
    var that = this;
    wx.showModal({
      // title: '提示',
      content: '确定退出登录',
      success(res) {
        if (res.confirm) {
          wx.removeStorageSync('token');
          wx.removeStorageSync('account');
          wx.removeStorageSync('client');
          wx.removeStorageSync('login_time');
          that.setData({
            info: ''
          })
          app.showToast("退出成功");
          setTimeout(() => {
            wx.navigateTo({
              url: '/pages/myCenter/login/login',
            })
          }, 2000) 
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
})