// pages/subpackage//pages/crm/backVisit/backVisit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeList: [{ name: '请选择工单状态', value: 0 }, { name: '无效工单', value: 1 },
    { name: '问题已处理', value: 2 }, { name: '前台已确认信息,待指派工程部', value: 3 }],
    tyepIndex: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  // 选工单状态
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tyepIndex: e.detail.value
    })
  },
  //提交的表单数据
  formSubmit(e) {
    console.log('提交的数据', e)
  }
})