// pages/subpackage//pages/crm/changeRecord/changeRecord.js
var app = getApp();
var pagea = 1;
var pageSize = 20;
var equipments_id='';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showSeachIcon: true,  //搜索_____________框搜索图标
    showCondition: false, //搜索_____________展示更多的搜索条件
    keyword: "",        //搜索_____________搜索关键字
    startTime: "",        //搜索_____________开始时间
    endTime: "",          //搜索_____________结束时间

    stockshistory: '',//库存记录列表
    hasMore: true, //是否有更多
    is_load: false,//是否加载
    loading: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options){
    equipments_id = options.id ? options.id : '';
  },
  onShow: function (options) {
    pagea = 1;
    this.setData({
      stockshistory: [],
      is_load: false,
      hasMore: true,
    })
    this.getStockChangeRecord();//获取列表
  },
  /*** 页面上拉加载*/
  onReachBottom: function () {
    var that = this;
    pagea += 1;
    // 当没有数据时,不再请求
    if (!that.data.hasMore) {
      return false;
    }
    that.setData({
      loading: true
    })
    that.getStockChangeRecord(); // 获取订单列表
  },
  /*** 下拉刷新*/
  onPullDownRefresh: function () {
    var that = this;
    pagea = 1;
    that.setData({
      stockshistory: [], // 订单列表
      hasMore: true, // 没有更多了状态
      is_load: false
    })
    that.getStockChangeRecord(); // 获取订单列表
  },
  //获取库存
  getStockChangeRecord() {
    var that = this;
    // ajax请求
    app.ajax({
      url: 'api/Equipments/getStockChangesRecord',
      url_type: 2,
      data: {
        page: pagea,
        pageSize: pageSize,
        startTime: that.data.startTime,
        endTime: that.data.endTime,
        key: that.data.keyword,
        equipments_id
      },
      success: function (res) {
        var newlist = '';//newlist(过渡)
        var oldlist = that.data.stockshistory;//原来的数组
        if (res.data.code == 1000) {
          if (oldlist.length <= 0) {
            newlist = res.data.data
          } else {
            var options = res.data.data;
            if (oldlist[oldlist.length - 1].date === options[0].date){
              var getdata = options.shift();
              var shenData = options;
              getdata.child && getdata.child.forEach((item,index)=>{
                oldlist[oldlist.length - 1].child.push(item);
              });
              newlist=oldlist.concat(shenData);

            }else{
              newlist = oldlist.concat(res.data.data);
            }
            
          }
          if (res.data.allPage <= pagea) {
            that.setData({
              hasMore: false
            })
          }
          that.setData({
            stockshistory: newlist
          })
          // console.log('最后的数据', that.data.stockshistory);
        } else {
          that.setData({
            hasMore: false
          })
        }
        //是否加载
        that.setData({
          is_load: true,
          loading: false
        })

        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },
  //搜索_____________输入框输入内容
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  //搜索_____________搜索框选中
  searchFocus() {
    this.setData({
      showSeachIcon: false,
    })
  },
  //搜索_____________搜索输入框清除图标
  inputClear() {
    this.setData({
      keyword: '',
      showSeachIcon: true
    })
  },
  //搜索_____________搜索失去焦点
  searchBlur(e) {
    const result = e.detail.value;
    if (result === "") {
      this.setData({
        showSeachIcon: true,
      })
    }
  },
  //搜索_____________点击筛选
  tapFilter() {
    let showCondition = this.data.showCondition;
    this.setData({
      showCondition: !showCondition,
    })
  },
  //搜索_____________隐藏筛选条件
  hideFilter() {
    this.setData({
      showCondition: false,
    })
  },
  //搜索_____________只搜索编号
  onlyKeywordber() {
    pagea = 1;
    this.setData({
      stockshistory: [],
      startTime: "",
      endTime: '',
      loading: true,
      havArea: '',
      is_load: false
    })
    this.getStockChangeRecord();//获取用户列表
  },
  //搜索_____________重置搜索条件
  searchReset() {
    var areaList = this.data.areaList;
    areaList.forEach(function (item, index) {
      item.checked = false;
    });
    this.setData({
      startTime: "",
      endTime: '',
      keyword: "",
      areaList: areaList,
      havArea: ''
    })
  },
  //搜索_____________搜索确定
  searchConfirm() {
    pagea = 1;
    this.setData({
      showCondition: false,
      loading: true,
      stockshistory: [],
      is_load: false
    })
    this.getStockChangeRecord();//获取用户列表
  },
  //搜索_____________开始时间
  startDateChange: function (e) {
    this.setData({
      startTime: e.detail.value
    })
  },
  //搜索_____________开始时间
  endDateChange: function (e) {
    this.setData({
      endTime: e.detail.value
    })
  },
})