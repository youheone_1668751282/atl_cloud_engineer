// pages/subpackage/pages/crm/certificationInfo/certificationInfo.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    auth_id: '',//认证id
    dataInfo: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      auth_id: options.id
    })
    this.getInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //获取详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'api/Realname/getRealnameDetail',
      url_type: 2,
      data: {
        auth_id: that.data.auth_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            dataInfo: res.data.data
          })
        }
      }
    })
  },
  //预览
  previewImage(e){
    wx.previewImage({
      current: e.currentTarget.dataset.img, // 当前显示图片的http链接
      urls: [this.data.dataInfo.front_idcard_pic, this.data.dataInfo.back_idcard_pic] // 需要预览的图片http链接列表
    })
  },
})