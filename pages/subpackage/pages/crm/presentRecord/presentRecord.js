// pages/subpackage/pages/crm/presentRecord/presentRecord.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    recordList: [],
    hasMore: false,
    loading: false,
    page: 1,
    row: 5,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;

    this.withdrawDepositRecord();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    if (!that.data.hasMore) {
      return false;
    }
    that.data.page++;
    that.setData({
      loading: true
    })
    that.withdrawDepositRecord();
  },


  // 获取提现记录
  withdrawDepositRecord() {
    var that = this;
    app.ajax({
      url: 'api/Withdraw/getWithdrawRecord',
      url_type: 2,
      data: {
        page: that.data.page,
        pageSize: that.data.row,
      },
      success: function(res) {
        console.log('res',res);
        if (res.data.code == 1000) {
          if (that.data.page == 1) {
            that.setData({
              recordList: res.data.data
            })
          } else {
            that.setData({
              recordList: that.data.recordList.concat(res.data.data)
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.count < that.data.pageSize) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
        } else {
          if (that.data.page == 1) {
            that.setData({
              hasMore: false,
              loading: false
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false
            })
          }
        }
      }
    })
  }
})
