// pages/subpackage/pages/crm/deviceAdd/deviceAdd.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getAreaMsg: '',//选择的地址信息
    address: '', //详细地址

    produceList: [{ equipments_id: '', equipments_name: '请选择产品名称' }],
    produceIndex: 0,//产品名称
    modeList: [{ name: '时长', value: 1 }, { name: '流量', value: 0, checked: 'true' }],//套餐模式
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 初始化动画变量
    var that = this;
    that.headid = this.selectComponent("#addressid"); //引入地址

    that.getProductList();//获取产品列表
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  // 提交表单
  submit(e){
    var that = this;
    var pageData = that.data;
    var detail = e.detail.value;
    if (detail.device_no==''){
      app.showToast('请输入设备编号');
      return
    }
    app.ajax({
      url: 'api/equipmentlists/add',
      url_type: 2,
      data: {
        device_no: detail.device_no,
        alias_name: detail.alias_name,
        equipments_id: pageData.produceList[pageData.produceIndex].equipments_id,
        working_mode: detail.package_mode,
        remaining_traffic: detail.package_mode == 1 ? 0 : detail.remaining_traffic,
        used_traffic: detail.package_mode == 1 ? 0 : detail.used_traffic,
        remaining_days: detail.package_mode == 1 ? detail.remaining_traffic : 0,
        used_days: detail.package_mode == 1 ? detail.used_traffic : 0,
        contact: detail.contact,
        contact_number: detail.contact_number,
        province: pageData.getAreaMsg ? pageData.getAreaMsg.areaName[0] : '',
        city: pageData.getAreaMsg ? pageData.getAreaMsg.areaName[1] : '',
        area: pageData.getAreaMsg ? pageData.getAreaMsg.areaName[2] : '',
        province_code: pageData.getAreaMsg ? pageData.getAreaMsg.areaId[0] : '',
        city_code: pageData.getAreaMsg ? pageData.getAreaMsg.areaId[1] : '',
        area_code: pageData.getAreaMsg ? pageData.getAreaMsg.areaId[2] : '',
        address: detail.address,
        // sim_value: detail.sim_value,
      },
      success: function (res) {
        app.showToast(res.data.msg, "none", 2000, function () { 
          if (res.data.code == 1000) {
            setTimeout(() => {
              wx.navigateBack();
            }, 1000)
          }
        });
      }
    })
  },
  //重置表单
  reset(){
    this.setData({
      modeList: [{ name: '时长', value: 1 }, { name: '流量', value: 0, checked: 'true' }],//套餐模式
      getAreaMsg: '',//选择的地址信息
      produceIndex: 0,
    })
  },

  //打开地图选择 
  openAddress() {
    var that = this;
    this.headid.startAddressAnimation(true);
  },
  //确定接受
  getSure(data) {
    console.log('确定事件获取地址', data);
    var that = this;
    let [province_code, city_code, area_code] = data.detail.areaId;
    let [province, city, area] = data.detail.areaName;
    that.setData({
      getAreaMsg: data.detail
    })
  },
  //选择产品
  bindProduceChange: function (e) {
    this.setData({
      produceIndex: e.detail.value,
      packageIndex: 0
    })
  },

  //获取产品列表
  getProductList() {
    var that = this;
    app.ajax({
      url: 'api/Equipments/getList',
      url_type: 2,
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var produceList = that.data.produceList;
          produceList = produceList.concat(res.data.data);
          that.setData({
            produceList
          })
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  },

  //套餐模式选择
  packageModeChange(e){
    var modeList = this.data.modeList;
    modeList.forEach(function (item, index) {
      if (item.value == e.detail.value) {
        item.checked = true;
      } else {
        item.checked = false;
      }
    });
    this.setData({
      modeList
    })
  },

})