// pages/subpackage//pages/crm/reportEdit/reportEdit.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',//报备id
    dataInfo: '',//报备详情
    user_id: '',//选择的客户id
    user_name: '',//选择的客户名称
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.id) {
      this.setData({
        id: options.id
      })
      this.getInfo();
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //获取报备详情
  getInfo(){
    var that = this;
    app.ajax({
      url: 'api/Report/reportDetails',
      url_type: 2,
      data: {
        id: that.data.id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            dataInfo: res.data.data,
            user_id: res.data.data.user_id,
            user_name: res.data.data.realname
          })
        }
      }
    })
  },
  //提交的表单数据
  formSubmit(e){
    console.log('提交的数据',e)
    var that = this;
    var value = e.detail.value;
    app.ajax({
      url: that.data.id ? 'api/Report/edit' : 'api/Report/add',
      url_type: 2,
      data: {
        id: that.data.id,
        username: value.name,
        phone: value.phone,
        id_card: value.idCard,
        user_id: that.data.user_id
      },
      success: function (res) {
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(()=>{
              wx.navigateBack();
            },1000)
          }
        })
      }
    })
  },
  navCustomerList(){
    wx.navigateTo({
      url: '../exchange/exchange?way=2',
    })
  }
})