// pages/subpackage/pages/crm/workOrderInfo/workOrderInfo.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    info: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id){
      this.setData({
        id: options.id
      })
      this.getInfo();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  //获取财务详情
  getInfo() {
    var that = this;
    app.ajax({
      url: 'api/Finance/getFinanceDetail',
      url_type: 2,
      data: {
        id: that.data.id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            info: res.data.data,
          })
        }
      }
    })
  },

  //跳转订单跟踪
  navOrderTracking(){
    wx.navigateTo({
      url: '../orderTracking/orderTracking',
    })
  },
})