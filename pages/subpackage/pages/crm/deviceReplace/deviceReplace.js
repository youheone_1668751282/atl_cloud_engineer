// pages/subpackage/pages/crm/deviceReplace/deviceReplace.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    device_old: '',//旧设备id
    device_no: '',//旧设备号
    user_name: '',//用户姓名
    user_tel: '',//用户电话

    device_new: '',//新设备号
    product: '',//新设备下发产品id
    product_name: '',
    product_type: '',//新设备下发产品类型
    type: '',//1智能设备2非智能设备
    package: '',//新设备下发套餐id
    package_name: '',
    note: '',//备注

    equipmentsList: [],//产品列表
    packageList: [],//套餐
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getProduct();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  //选择设备
  navDeviceList() {
    wx.navigateTo({
      url: '../deviceManage/deviceManage?way=2',
    })
  },

  //获取产品
  getProduct(){
    var that = this;
    app.ajax({
      url: 'api/Equipments/getList',
      url_type: 2,
      data: {
        pageSize: 9999
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            equipmentsList: res.data.data
          })
        }
      }
    })
  },
  //选择下发的产品
  bindProduct(e) {
    var product_id = this.data.equipmentsList[e.detail.value].equipments_id;
    var product_name = this.data.equipmentsList[e.detail.value].equipments_name;
    var product_type = this.data.equipmentsList[e.detail.value].equipments_type;
    var type = this.data.equipmentsList[e.detail.value].type;
    this.setData({
      product: product_id,
      product_name: product_name,
      product_type: product_type,
      type: type,
      packageList: [],
      package: '',//新设备下发套餐id
      package_name: '',
    })
    this.getPackage(product_id);
  },

  //获取套餐
  getPackage(id) {
    var that = this;
    app.ajax({
      url: 'api/Equipmentlists/productToPackage',
      url_type: 2,
      data: {
        id: id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            packageList: res.data.data.package
          })
        }
      }
    })
  },
  //选择套餐
  bindPackage(e){
    var package_id = this.data.packageList[e.detail.value].package_id;
    var package_name = this.data.packageList[e.detail.value].package_name;
    this.setData({
      package: package_id,
      package_name: package_name,
    })
  },

  //新设备号输入
  devicenewInput(e){
    this.setData({
      device_new: e.detail.value
    })
  },
  //校验设备编号
  checkDevice(){
    var that = this;
    if (!that.data.device_new) {
      app.showToast('请输入新设备号');
      return
    }
    app.ajax({
      url: 'api/Equipmentlists/checkDevice',
      url_type: 2,
      data: {
        device_no: that.data.device_new
      },
      success: function (res) {
        app.showToast(res.data.msg);
      }
    })
  },

  //提交的表单数据
  formSubmit(e) {
    console.log('提交的数据', e)
    var that = this;
    var value = e.detail.value;
    if (!that.data.device_old) {
      app.showToast('请选择旧设备');
      return
    } else if (!value.device_new && that.data.type != 2) {
      app.showToast('请输入新设备号');
      return
    } else if (!that.data.product) {
      app.showToast('请选择下发的产品');
      return
    } else if (that.data.product_type==1 && !that.data.package) {
      app.showToast('请选择套餐');
      return
    }
    var disable = false;
    if (disable){
      return
    }
    app.ajax({
      url: 'api/Equipmentlists/doReplaceDevice',
      url_type: 2,
      data: {
        device_one: that.data.device_old,
        device_two: value.device_new,
        product: that.data.product,
        package: that.data.package,
        note: value.remark
      },
      success: function (res) {
        disable = true;
        setTimeout(()=>{
          disable = false;
        },2000)
        app.showToast(res.data.msg, 'none', 2000, function () {
          if (res.data.code == 1000) {
            setTimeout(() => {
             // wx.navigateBack();
              wx.redirectTo({
                url: '/pages/subpackage/pages/crm/deviceEdit/deviceEdit?equipment_id=' + that.data.device_old,
              })
            }, 1000)
          }
        })
      }
    })
  },
  //表单重置
  formReset(){
    this.setData({
      device_old: '',//旧设备号
      user_name: '',//用户姓名
      user_tel: '',//用户电话
      device_new: '',//新设备号
      product: '',//新设备下发产品id
      product_name: '',
      package: '',//新设备下发套餐id
      package_name: '',
      note: '',//备注
    })
  },
})