// pages/subpackage//pages/crm/transfer/transfer.js
var app=getApp();
var stock_id = '',role='',uid='';
var allData = { admin: [], workers: [], opera: [] };
var superior = [];
Page({

  /**
   * 页面的初始数据
   */
  data: {
    multiArray: [[], []],//多选列表
    multiIndex:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    stock_id = options.id ? options.id : '';
    this.getSelectsData();
  },
  //获取收货人
  getSelectsData(e) {
    var that = this;
    app.ajax({
      url: 'api/Equipments/getSelectData',
      url_type: 2,
      data: {},
      success: function (res) {
        if(res.data.code==1000){
            var getData=res.data.data;
                allData = { admin: [], workers: [], opera:[]};//重置
                superior=[];//重置
          var data = {
            multiArray: that.data.multiArray
          };
          //运营中心
          if (getData.operation.length > 0) {
              getData.operation.forEach((item, index) => {
                allData.opera.push({ id: item.o_id, name: item.company_name });
              })
              superior.push({ id: 2, name: '运营中心' })
              //设置初始值
            data.multiArray[1] = allData.opera;
          } 
          //合伙人 给的数据无语
          if (getData.administrative.length>0){
              getData.administrative.forEach((item,index)=>{
                allData.admin.push({ id: item.a_id, name: item.company_name});
              })
              superior.push({ id: 3, name:'合伙人'})
              //设置初始值
              if (getData.oa_workers.length <= 0){
                  data.multiArray[1] = allData.admin;
              }
          } 
          //经销商 
          if (getData.oa_workers.length > 0){
            getData.oa_workers.forEach((item, index) => {
              allData.workers.push({ id: item.workers_id, name: item.workers_name});
              })
              superior.push({ id: 4, name: '经销商' })
              //设置初始值
              if (getData.oa_workers.length <= 0 && getData.administrative.length <= 0) {
                data.multiArray[1] = allData.workers;
              }
          }
          data.multiArray[0] = superior;
          that.setData(data);
        }else{
          app.showToast(res.data.msg);
        }
      }
    })
  },
  //划拨的人确定事件
  bindMultiPickerChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value
    })
  },
  //确定选中的改变事件
  bindMultiPickerColumnChange: function (e) {
    // console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex.length <= 1 ? [0, 0] : this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = allData.opera;
            break;
          case 1:
            data.multiArray[1] = allData.admin;
            break;
          case 2:
            data.multiArray[1] = allData.workers;
            break;
        }
        data.multiIndex[1] = 0;
        break;
    }
    //给 role 和  uid赋值
    var currArr0 = data.multiArray[0];
    var currArr1 = data.multiArray[1];
    role = currArr0[data.multiIndex[0]].id ;//+ currArr0[data.multiIndex[0]].name;
    uid = currArr1[data.multiIndex[1]].id;// + currArr1[data.multiIndex[1]].name; console.log('选中的id', role, uid)
    this.setData(data);
  },
  //提交数据
  formSubmit(e){
    var that = this;
    var change_volume = e.detail.value.nums;
    var remark = e.detail.value.remarks;
      console.log('showdata', uid, role);
    app.ajax({
      url: 'api/Equipments/transfer_stock',
      url_type: 2,
      data: {
        stock_id,
        change_volume,
        remark,
        role,
        uid
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.msg);
          setTimeout(() => {
            wx.navigateBack({
              delta: 1,
            })
          }, 1000)
        } else {
          app.showToast(res.data.msg);
        }
      }
    })
  }
})