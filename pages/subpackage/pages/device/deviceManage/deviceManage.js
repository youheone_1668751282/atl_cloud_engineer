// pages/subpackage/pages/device/deviceManage/deviceManage.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasMore: false,
    loading: false,
    loadShow: false,
    empty: true,
    equipment: [],
    searchKey: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    console.log("1111")
    _PAGE = 1;
    that.getDevice();
    // that.setData({
    //   loadShow: false
    // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getDevice();
  },


  // 输入搜索字段
  inputSearch: function (e) {
    var that = this;
    var searchKey = e.detail.value;
    that.setData({
      searchKey: searchKey
    });
  },
  //小键盘直接搜索
  searchOrder: function (e) {
    var that = this;
    var key = e.detail.value;
    _PAGE = 1;
    that.setData({
      equipment: [],
      searchKey: key,
      empty: true,
      hasMore: false,
      loading: false,
      loadShow: false
    })
    that.getDevice();
  },
  //点击搜索图标搜索
  searchOrder2: function () {
    var that = this;
    _PAGE = 1;
    that.setData({
      equipment: [],
      empty: true,
      hasMore: false,
      loading: false,
      loadShow: false
    })
    that.getDevice();
  },
  //获取设备列表
  getDevice() {
    var that = this;
    app.ajax({
      url: 'Engineer/Equipmentlists/engineerEquipment',
      data: { 
        keywords: that.data.searchKey,
        page: _PAGE, 
        pageSize: _PAGESIZE 
      },
      success: function (res) {
        wx.stopPullDownRefresh();
        if (res.data.code == 1000) {
          //处理数据
          if (_PAGE == 1) {
            that.setData({
              equipment: res.data.data,
              empty: false
            });
          } else {
            that.setData({
              equipment: that.data.equipment.concat(res.data.data),
              empty: false
            });
          }
          //是否需要加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          })
        }else{
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
          
      }      
      
    })
  },
  //跳转设备详情
  deviceDetail(e) {
    var equipment_id = e.currentTarget.dataset.equipment_id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/deviceDetail/deviceDetail?equipment_id=' + equipment_id, //equipment_id,
    })
  },

  //更换机型
  changeModel(e){
    var equipment_id = e.currentTarget.dataset.equipment_id;
    var contract_id = e.currentTarget.dataset.contract_id;
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/changeModel/changeModel?equipment_id=' + equipment_id + '&contract_id=' + contract_id, 
    })
  }

})
