// pages/remoteControl/remoteControl.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: '',

    wash_regularly: 2,//定时冲洗，单位h，默认2
    screen_mode: 1,//1：已用天数、已用流量，2：已用天数、剩余流量，3：剩余天数、已用流量，4：剩余天数、剩余流量'
    screenList: [],//显示配置
    timedFlList: [],//冲洗配置

    open: false,//开关
    openfirst: true,//防止频繁点击
    screen: false,//显屏
    screenfirst: true,//防止频繁点击

    isShare: true,
    forced_flushing_time: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      equipment_id: options.equipment_id
    })
    that.equipmentDetail();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.screenMode();//获取屏显模式配置
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 设备详情
   */
  equipmentDetail: function () {
    var that = this;
    var equipment_id = that.data.equipment_id;
    app.ajax({
      url: "User/Equipmentlists/equipmentDetail",
      data: { "equipment_id": equipment_id },
      success: function (res) {
        wx.stopPullDownRefresh()
        if (res.data.code == 1000) {
          that.setData({
            info: res.data.data,
            open: res.data.data.switch_machine==1?true:false
          })
        }
        that.timedFlush(res.data.data.wash_regularly);//获取定时冲洗配置
        that.setData({
          is_load: true
        })
      }
    })
  },
  //获取定时冲洗配置
  timedFlush(wash_reguss) {
    var that = this;
    app.ajax({
      url: "Common/Conf/timedFlush",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          var havValue = false, arrshow = res.data.data;
          for (var i = 0; i < arrshow.length; i++) {
            if (arrshow[i].value == wash_reguss) {
              havValue = true;
            }
          }
          var obj = { 'description': wash_reguss + '小时冲洗一次', 'value': wash_reguss };
          if (!havValue) {
            arrshow.push(obj);
          }
          that.setData({
            timedFlList: arrshow
          })
        }
      }
    })
  },

  //开关
  chooseone(e) {
    var that = this, opes = e.currentTarget.dataset.types, openfirst = that.data.openfirst;
    var type = that.data.info.type;
    var status = that.data.info.status;
    if (type != 1) {
      app.showToast("您的设备非智能设备无法使用该功能", 'none', 2500);
      return false;
    }
    if (status == 1) {
      app.showToast("您的设备已离线，无法进行该操作", 'none', 2500);
      return false;
    }
    if (openfirst == false) {
      app.showToast("为保护设备请勿频繁操作,请5秒后重试", 'none', 3000);
      return false;
    }
    if (opes == false) {
      that.powerOff(0)
      that.setData({
        open: true,
        openfirst: false,
      })
    } else {
      that.powerOff(1)
      that.setData({
        open: false,
        openfirst: false,
      })
    }
  },
  //冲洗
  choosetwo(e) {
    // app.showToast("冲洗成功",'none',2500);
    var that = this;
    var sn = that.data.info.device_no;
    var type = that.data.info.type;
    var status = that.data.info.status;
    if (type != 1) {
      app.showToast("您的设备非智能设备无法使用该功能", 'none', 2500);
      return false;
    }
    if (status == 1) {
      app.showToast("您的设备已离线，无法进行该操作", 'none', 2500);
      return false;
    }
    if (!that.data.open) {
      app.showToast("请先开启设备", 'none', 2500);
      return false;
    }
    app.ajax({
      url: 'House/Issue/mandatory_rinse',
      data: { "sn": sn },
      success: function (res) {
        var forced_flushing_time = that.data.info.forced_flushing_time;
        that.setData({
          forced_flushing_time: forced_flushing_time,
          isShare: false
        })
        wx.vibrateLong();
        var timer = setInterval(function () {

          forced_flushing_time--;
          that.setData({
            forced_flushing_time: forced_flushing_time
          })
          if (forced_flushing_time <= 0) {
            clearInterval(timer);
            that.setData({
              isShare: true,
              forced_flushing_time: 0
            })
            app.showToast('亲,您的设备已经干净了~', 'none', 2500);
          }
          //console.log(forced_flushing_time)
        }, 1000)
      }
    })
  },
  //显屏
  choosethree(e) {
    var that = this, sceend = e.currentTarget.dataset.types, screenfirst = that.data.screenfirst;
    var type = that.data.info.type;
    var status = that.data.info.status;
    if (type != 1) {
      app.showToast("您的设备非智能设备无法使用该功能", 'none', 2500);
      return false;
    }
    if (status == 1) {
      app.showToast("您的设备已离线，无法进行该操作", 'none', 2500);
      return false;
    }
    if (!that.data.open) {
      app.showToast("请先开启设备", 'none', 2500);
      return false;
    }
    if (screenfirst == false) {
      app.showToast("为保护设备请勿频繁操作,请5秒后重试", 'none', 3000);
      return false;
    }
    if (sceend == false) {
      that.screen_status(0)
      that.setData({
        screen: true,
        screenfirst: false,
      })
    } else {
      that.screen_status(1)
      that.setData({
        screen: false,
        screenfirst: false,
      })
    }
  },
  //下发屏幕开关
  screen_status(screen_status) {
    var that = this;
    var screen_status = screen_status;
    var sn = that.data.info.device_no;
    app.ajax({
      url: 'House/Issue/screen_status',
      data: { "sn": sn, "screen_status": screen_status },
      success: function (res) {
        var msg;
        if (screen_status == 0) {
          msg = '设备屏显已开启';
        } else {
          msg = '设备屏显已关闭';
        }
        wx.vibrateLong();
        app.showToast(msg, 'none', 2500);
        setTimeout(() => {
          that.setData({
            screenfirst: true
          })
        }, 5000);
      }
    })
  },
  //发送指令
  //开关机
  powerOff(device_status) {
    var that = this;
    var device_status = device_status;
    var sn = that.data.info.device_no;
    app.ajax({
      url: 'House/Issue/power_off',
      data: { "sn": sn, "device_status": device_status },
      success: function (res) {
        var msg;
        // if (device_status == 1) {
        //   msg = '设备已开启';
        // } else {
        //   msg = '设备已关闭';
        // }
        wx.vibrateLong();
       // app.showToast(msg, 'none', 2500);
        setTimeout(() => {
          that.setData({
            openfirst: true
          })
        }, 5000);
        return false;
      }

    })
  },
  //定时冲洗
  choosefour(e) {
    var that = this, sn = that.data.info.device_no,
      index = e.currentTarget.dataset.index;
    var type = that.data.info.type;
    var status = that.data.info.status;
    if (type != 1) {
      app.showToast("您的设备非智能设备无法使用该功能", 'none', 2500);
      return false;
    }
    if (status == 1) {
      app.showToast("您的设备已离线，无法进行该操作", 'none', 2500);
      return false;
    }
    if (!that.data.open) {
      app.showToast("请先开启设备", 'none', 2500);
      return false;
    }
    var timedFlList = that.data.timedFlList;
    if ((index + 1) < timedFlList.length) {
      index += 1;
    } else {
      index = 0;
    }
    //console.log('index', index, 'length', timedFlList.length, "这是谁?", timedFlList[index].value)
    app.ajax({
      url: "House/Issue/modified_timed_washing",
      data: { sn: sn, value: timedFlList[index].value },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(parseInt(timedFlList[index].value) + '小时冲洗一次', 'none', 2500);
          that.setData({
            wash_regularly: timedFlList[index].value
          })
        } else {
          app.showToast(res.data.message, 'none', 2500);
        }
      }
    })
  },
  //切换屏显
  choosefive(e) {
    var that = this, sn = that.data.info.device_no, index = e.currentTarget.dataset.index;
    var type = that.data.info.type;
    var status = that.data.info.status;
    if (type != 1) {
      app.showToast("您的设备非智能设备无法使用该功能", 'none', 2500);
      return false;
    }
    if (status == 1) {
      app.showToast("您的设备已离线，无法进行该操作", 'none', 2500);
      return false;
    }
    if (!that.data.open) {
      app.showToast("请先开启设备", 'none', 2500);
      return false;
    }
    var screenList = that.data.screenList;
    if (index < screenList.length) {
      index += 1;
    } else {
      index = 1;
    }
    //console.log('index', index, 'length', screenList.length, "上传的结果?", screenList[index-1].time_mode,screenList[index-1].flow_mode)
    app.ajax({
      url: "House/Issue/switch_screen_display_mode",
      data: { sn: sn, value: [screenList[index - 1].time_mode, screenList[index - 1].flow_mode] },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast('模式' + ' ' + screenList[index - 1].description, 'none', 3000);
          that.setData({
            screen_mode: index
          })
        } else {
          app.showToast(res.data.message, 'none', 2500);
        }
      }
    })

  },
  //获取屏显模式配置  
  screenMode() {
    var that = this;
    app.ajax({
      url: "Common/Conf/screenMode",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            screenList: res.data.data
          })
        }
      }
    })
  },
})