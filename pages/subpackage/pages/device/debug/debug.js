// pages/device/debug/debug.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    device_no:'',
    device_info:{},
    screenList:[],
    screen_mode:0,
    screen_mode_txt:'',
    forced_flushing_time:0,
    isShare:true,
    force_wash_modal:true,
    force_wash_focus: false,
    force_wash_time:0,
    force_wash_time_input:0,
    timedFlList:[],
    wash_regularly:0,
    wash_regularly_index:0,
    maintenance_duration:0,
    maintenance_modal:true,
    maintenance_focus: false,
    maintenance_time: 0,
    maintenance_input: 0,
    switchColor1:'#04BE02',
    switchColor2: 'gray',
    disabled: '',
    color:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      device_no: options.device_no
    })
  },
  onShow:function(){
    var that = this;
    that.detail();
    that.screenMode();
    that.timedFlush();
    
  },
  onPullDownRefresh:function(){
    var that = this;
    that.detail();
  },
  //设备详情
  detail(){
    var that = this;
    app.ajax({
      url: 'Engineer/Equipmentlists/detail',
      data: { "device_no": that.data.device_no},
      success: function (res) {
        console.log(res.data.data)
        if (res.data.code == 1000) {
          var disabled = false;
          var color = '';
          if (res.data.data.switch_machine==1){
            disabled = false;
            color = that.data.switchColor1;
          }else{
            disabled = true;
            color = that.data.switchColor2;
          }
          that.setData({
            device_info:res.data.data,
            screen_mode_txt: res.data.data.screen_mode_txt,
            force_wash_time: res.data.data.forced_flushing_time,
            wash_regularly: res.data.data.wash_regularly,
            maintenance_duration: res.data.data.maintenance_duration,
            disabled: disabled,
            color: color
          })
        } else {
          app.showToast(res.data.message);
          setTimeout(function(){
            wx.navigateBack({
              delta:1
            })
          },2000)
          
          return false;
        }
        wx.hideLoading();
        wx.stopPullDownRefresh();
      }
    })
  },
  // 获取心跳
  syncRedisToDevice(){
    var that = this;
    app.ajax({
      url: "House/Issue/syncRedisToDevice",
      data: {
        sn: that.data.device_no
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast('获取成功,2s后自动刷新');
          setTimeout(function(){
            wx.showLoading({
              title: '刷新中...',
            });
            that.detail();
          },2000)
        }else{
          app.showToast(res.data.msg);
        }
      }
    })
    
  },
  // 更新滤芯
  filterUpdate(){
    var that = this;
    wx.showLoading({
      title: '更新中...',
    })
    app.ajax({
      url: "House/Issue/filter_element",
      data: {
        sn: that.data.device_no
      },
      success: function (res) {
        if (res.data.code == 1000) {
          setTimeout(function(){
            wx.showLoading({
              title: '获取心跳中...',
            })
            that.syncRedisToDevice();
          },2000)
        } else {
          app.showToast(res.data.msg);
        }
      }
    })

  },
  //获取屏显模式配置  
  screenMode() {
    var that = this;
    app.ajax({
      url: "Common/Conf/screenMode",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            screenList: res.data.data
          })
        }
      }
    })
  },
  force_wash_input: function (e) {
    var that = this;
    var force_wash_time_input = e.detail.value;
    console.log('force_wash_time_input', force_wash_time_input);
    that.setData({
      force_wash_time_input: force_wash_time_input

    })
  },
  // 取消
  force_wash_cancel: function (e) {
    var that = this;
    that.setData({
      force_wash_modal: true,
    })
  },
  //确认修改强制冲洗时间
  force_wash_confirm(){
    var that = this;
    app.ajax({
      url: 'House/Issue/modify_mandatory_flushing_times',
      data: { "sn": that.data.device_no, "value": that.data.force_wash_time_input },
      success: function (res) {
        app.showToast(res.data.message);
        if (res.data.code == 1000) {
          that.setData({
            force_wash_time: that.data.force_wash_time_input,
            force_wash_modal: true
          })
        } 
      }
    })
  },
  //修改检修参数输入
  maintenance_input: function (e) {
    var that = this;
    var maintenance_input = e.detail.value;
    that.setData({
      maintenance_input: maintenance_input

    })
  },
  // 取消
  maintenance_cancel: function (e) {
    var that = this;
    that.setData({
      maintenance_modal: true,
    })
  },
  //确认修改检修参数
  maintenance_confirm() {
    var that = this;
    app.ajax({
      url: 'House/Issue/modify_maintenance_parameters',
      data: { "sn": that.data.device_no, "value": that.data.maintenance_input },
      success: function (res) {
        app.showToast(res.data.message);
        if (res.data.code == 1000) {
          that.setData({
            maintenance_duration: that.data.maintenance_input,
            maintenance_modal: true
          })
        }
      }
    })
  },
 //测试模式-开关，0：开启，1：关闭
  debugMode(e){
    var val = e.detail.value;
    var value = 1;
    var msg = "关闭";
    if(val){
      value = 0;
      msg = "开启";
    }
    var that = this;
    var check = that.checkDeviceStatus();
    if(!check){
      return false;
    }
    app.ajax({
      url: 'House/Issue/switch_test_mode',
      data: { "sn": that.data.device_no, "value": value},
      success:function(res){
        if(res.data.code==1000){
          app.showToast(msg+"测试模式成功");
        }else{
          app.showToast(res.data.message);
        }
      }
    })
  },
  //设备开关 开关机，0：开，1：关闭
  switchSwitch(e){
    var that = this;
    var val = e.detail.value;
    var value = 1;
    var msg = "设备关机";
    var disabled = true;
    var color = that.data.switchColor2;
    if (val) {
      value = 0;
      msg = "设备开机";
      disabled = false;
      color = that.data.switchColor1;
    }
    app.ajax({
      url: 'House/Issue/power_off',
      data: { "sn": that.data.device_no, "device_status": value },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(msg + "成功");
          that.setData({
            disabled: disabled,
            color: color
          })
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //屏显开关 屏幕状态，0：开，1：关闭
  screenSwitch(e){
    var val = e.detail.value;
    var value = 1;
    var msg = "关闭屏幕";
    if (val) {
      value = 0;
      msg = "开启屏幕";
    }
    var that = this;
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    app.ajax({
      url: 'House/Issue/screen_status',
      data: { "sn": that.data.device_no, "screen_status": value },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(msg + "成功");
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //检修开关 状态，0：解除检修，1：进入检修
  repairSwitch(e){
    var val = e.detail.value;
    var value = 0;
    var msg = "停止检修";
    if (val) {
      value = 1;
      msg = "进入检修";
    }
    var that = this;
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    app.ajax({
      url: 'House/Issue/maintenance_state_switching',
      data: { "sn": that.data.device_no, "value": value },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(msg + "成功");
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //切换屏显
  switchScreenMode(e){
    var that = this, sn = that.data.device_no, index = e.currentTarget.dataset.index;
    var screenList = that.data.screenList;
    if (index < screenList.length) {
      index += 1;
    } else {
      index = 1;
    }
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    app.ajax({
      url: "House/Issue/switch_screen_display_mode",
      data: { sn: sn, value: [screenList[index - 1].time_mode, screenList[index - 1].flow_mode] },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast('模式' + ' ' + screenList[index - 1].description, 'none', 3000);
          that.setData({
            screen_mode: index,
            screen_mode_txt: screenList[index - 1].description
          })
        } else {
          app.showToast(res.data.message, 'none', 2500);
        }
      }
    })
  },
  //强制冲洗
  forceWash(){
    var that = this;
    var sn = that.data.device_no;
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    app.ajax({
      url: 'House/Issue/mandatory_rinse',
      data: { "sn": sn },
      success: function (res) {
        var forced_flushing_time = that.data.device_info.forced_flushing_time;
        that.setData({
          forced_flushing_time: forced_flushing_time,
          isShare: false

        })
        wx.vibrateLong();
        var timer = setInterval(function () {
          forced_flushing_time--;
          that.setData({
            forced_flushing_time: forced_flushing_time
          })
          if (forced_flushing_time <= 0) {
            clearInterval(timer);
            that.setData({
              forced_flushing_time: 0,
              isShare: true
            })
            app.showToast('亲,您的设备已经干净了~', 'none', 2500);
          }
          //console.log(forced_flushing_time)
        }, 1000)
      }
    })
  },
  //远程重启
  remoteReboot(){
    var that = this;
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    app.ajax({
      url: 'House/Issue/remote_restart',
      data: { "sn": that.data.device_no},
      success: function (res) {
        app.showToast(res.data.message);
      }
    })
  },
  //修改强制冲洗时间
  edit_force_wash(){
    var that = this;
    // var check = that.checkDeviceStatus();
    // if (!check) {
    //   return false;
    // }
    that.setData({
      force_wash_focus: true,
      force_wash_modal:false,
      force_wash_time_input: '',
    })
  },
  //修改检修参数
  edit_maintenance(){
    var that = this;
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    that.setData({
      maintenance_focus: true,
      maintenance_modal: false,
      maintenance_input: ''
    })
  },
  //获取定时冲洗配置
  timedFlush() {
    var that = this;
    app.ajax({
      url: "Common/Conf/timedFlush",
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            timedFlList: res.data.data
          })
        }
      }
    })
  },
  //修改定时冲洗参数
  edit_time_wash(e){
    var that = this, sn = that.data.device_no,
    index = e.currentTarget.dataset.index;
    var check = that.checkDeviceStatus();
    if (!check) {
      return false;
    }
    var timedFlList = that.data.timedFlList;
    if ((index + 1) < timedFlList.length) {
      index += 1;
    } else {
      index = 0;
    }
    app.ajax({
      url: "House/Issue/modified_timed_washing",
      data: { sn: sn, value: timedFlList[index].value },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(timedFlList[index].value + '小时冲洗一次', 'none', 2500);
          that.setData({
            wash_regularly: timedFlList[index].value,
            wash_regularly_index:index
          })
        } else {
          app.showToast(res.data.message, 'none', 2500);
        }
      }
    })
  },
  //判断设备状态
  checkDeviceStatus(){
    var that = this;
    if (that.data.device_info.status==1){
      app.showToast("该设备已离线,无法进行操作");
      return false;
    }
    if (that.data.device_info.switch_machine == 0) {
      app.showToast("该设备已关机,无法进行操作");
      return false;
    }
    return true;
  }


  
})