// pages/import/import.js
var util = require("../../../utils/util.js");
var app = getApp();
var moreup = false;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    values:'',
    scanning: '/images/scanning.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    moreup = false;
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  //input 输入框
  inputfun(e){
    var that=this;
    that.setData({
      values: e.detail.value
    })
  },
  // 扫码
  scanning: function () {
    var that = this;
    app.scanning(function (data){
      that.setData({
        values: data
      })
    })
  },
  //确认事件 设备信息
  affirmfun(e){
    var that = this;
    if(that.data.values == '' || that.data.values == null){
      app.showToast("请输入设备编号","none",2000,function(){});
      return;
    }
    if(moreup){
      return false
    }
    moreup=true;
    var device_no = that.data.values;
    // 判断设备是否存在
    app.ajax({
      url: 'Engineer/Equipmentlists/detail',
      data: { "device_no": device_no },
      success: function (res) {
        
        if (res.data.code == 1000) {
          wx.navigateTo({
            url: '/pages/subpackage/pages/device/debug/debug?device_no=' + device_no,
          })
        } else {
          app.showToast(res.data.message);
          moreup = false;
          return false;
        }
      }
    })
    
    
  }
})