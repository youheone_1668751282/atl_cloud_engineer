// pages/order/emergencyContact/emergencyContact.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',//姓名
    telphone: '',//电话
    user_id: '',//用户id
    work_order_id: '',//工单id
    emergency_contact: '',//紧急联系人
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var user_id = options.user_id;
    var work_order_id = options.work_order_id;
    var emergency_contact = JSON.parse(options.emergency_contact);
    console.log('user_id', user_id);
    console.log('work_order_id', work_order_id);
    console.log('emergency_contact', emergency_contact);
    that.setData({
      user_id: user_id,
      work_order_id: work_order_id,
      name: emergency_contact.name||'',
      telphone: emergency_contact.tel||''
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  // 监听输入联系人称呼
  inputName(e){
    var that = this;
    var value = e.detail.value;
    that.setData({
      name: value
    })
  },
  // 监听输入联系人电话
  inputTel(e) {
    var that = this;
    var value = e.detail.value;
    that.setData({
      telphone: value
    })
  },
  // 提交保存
  saveForm(e){
    var that = this;
    var name = that.data.name;
    var telphone = that.data.telphone;
    var user_id = that.data.user_id;
    var work_order_id = that.data.work_order_id;
    var emergency_contact = {
        name: name,
        tel: telphone
    }
//    app.saveFormId(e.detail.formId);
    if(name == ''){
      app.showToast('请输入紧急联系人称呼');
      return;
    }
    if(telphone == ''){
      app.showToast('请输入紧急联系人电话');
      return;
    }
    app.ajax({
      url: 'Engineer/User/editContacts',
      data:{
        user_id: user_id,
        work_order_id: work_order_id,
        emergency_contact: emergency_contact
      },
      success(res){
        if(res.data.code == 1000){
          app.showToast('提交成功,2s后跳转');
          setTimeout(function(){
            wx.navigateBack({
              delta: 1
            })
          },2000)
        }else{
          app.showToast(res.data.message);
        }
      }
    })
  }
})