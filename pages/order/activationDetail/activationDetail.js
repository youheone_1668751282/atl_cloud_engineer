// pages/device/debug/debug.js
var app = getApp();
let disabled = '', color = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    device_no: '',
    device_info: {},
    switchColor1: '#04BE02',
    switchColor2: 'gray',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      device_no: options.device_no
    })
  },
  onShow: function () {
    var that = this;
    that.detail();
  },
  onPullDownRefresh: function () {
    var that = this;
    that.detail();
  },
  //设备详情
  detail() {
    var that = this;
    app.ajax({
      url: 'Engineer/Equipmentlists/detail',
      data: { "device_no": that.data.device_no },
      success: function (res) {
        console.log(res.data.data)
        if (res.data.code == 1000) {
          if (res.data.data.switch_machine == 1) {
            disabled = false;
            color = that.data.switchColor1;
          } else {
            disabled = true;
            color = that.data.switchColor2;
          }
          that.setData({
            device_info: res.data.data,
          })
        } else {
          app.showToast(res.data.message);
          setTimeout(function () {
            wx.navigateBack({
              delta: 1
            })
          }, 2000)

          return false;
        }
        wx.hideLoading();
        wx.stopPullDownRefresh();
      }
    })
  },
  // 获取心跳
  syncRedisToDevice() {
    var that = this;
    app.ajax({
      url: "House/Issue/syncRedisToDevice",
      data: {
        sn: that.data.device_no
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast('获取成功,2s后自动刷新');
          setTimeout(function () {
            wx.showLoading({
              title: '刷新中...',
            });
            that.detail();
          }, 2000)
        } else {
          app.showToast(res.data.msg);
        }
      }
    })

  },
  //设备开关 开关机，0：开，1：关闭
  switchSwitch(e) {
    var that = this;
    var val = e.detail.value;
    var value = 1;
    var msg = "设备关机";
    var is_disabled = true;
    var is_color = that.data.switchColor2;
    if (val) {
      value = 0;
      msg = "设备开机";
      is_disabled = false;
      is_color = that.data.switchColor1;
    }
    app.ajax({
      url: 'House/Issue/power_off',
      data: { "sn": that.data.device_no, "device_status": value },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(msg + "成功");
          disabled = is_disabled;
          color = is_color;
        } else {
          app.showToast(res.data.message);
        }
      }
    })
  },
  //远程重启
  remoteReboot() {
    var that = this;
    app.ajax({
      url: 'House/Issue/remote_restart',
      data: { "sn": that.data.device_no },
      success: function (res) {
        app.showToast(res.data.message);
      }
    })
  },
  //重新下发套餐
  IssuePackage(e) {
    var that = this;
    wx.showModal({
      title: '提示',
      content: '是否需要重新下发套餐？',
      success(res) {
        if (res.confirm) {
          that.setData({
            issue_falg: true
          })
          that.issue(that.data.device_info.contract_id, that.data.device_info.equipment_id);
        }
        if (res.cancel) {

        }
      },
    })
  },
  //下发套餐接口
  issue(contract_id, equipment_id) {
    var that = this;
    app.ajax({
      url: "Common/Contract/issuePackage",
      data: { device_no: that.data.device_no},
      success: function (res) {
        app.showToast(res.data.message);
        if (res.data.code == 1000) {
          that.detail();
        }
      }
    })
  },
  //恢复出厂设备
  resumeFactory(){
    var that = this;
    app.ajax({
      url: 'House/Issue/factory_data_reset',
      data: { "sn": that.data.device_no },
      success: function (res) {
        app.showToast(res.data.message);
      }
    })
  }



})