// pages/order/confirmAddress/confirmAddress.js
var app = getApp();
var page_size = 20;
var page_index = 1; //默认页码
Page({
  /**
   * 页面的初始数据
   */
  data: {
    con_signee: '', //联系人
    mo_bile: '', //手机号码
    add_ress: '', //详细地址
    is_default: false, //默认不是默认地址
    order_id: '', //单个订单id
    ch_latitude: '', //当前
    ch_longitude: '', //当前
    numberPlate: '',
    house_type: 1,
    isSubimit: false, //避免重复提交
    type: '',//进入页面类型，2：移机进入，1：其他进入
    valueName: [],//设置回显选择的地址(name)
    getAreaMsg: {},//选择的地址信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 初始化动画变量
    var that = this;
    var order_id = options.order_id;
    that.headid = this.selectComponent("#addreid"); //引入地址
    that.setData({
      order_id: order_id,
      type: options.type
    })
    that.getAddress();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  // 获取订单详情--地址信息
  getAddress(){
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/workOrderDetail',
      method: 'POST',
      data:{
        work_order_id: that.data.order_id
      },
      success(res){
        if(res.data.code==1000){
          var type = that.data.type;
          var newArrname = [];
          var newArrid = [];
          var service_address = '';
          var numberPlate = '';
          var lat = '';
          var lng = '';

          if(type == 1){
            newArrname = [res.data.data.province, res.data.data.city, res.data.data.area];
            newArrid = [res.data.data.province_code, res.data.data.city_code, res.data.data.area_code];
            service_address = res.data.data.service_address;
            numberPlate = res.data.data.numberPlate ? res.data.data.numberPlate : '';
            lat = res.data.data.lat;
            lng = res.data.data.lng;
            
          }else if(type == 2){
            newArrname = [res.data.data.move_province, res.data.data.move_city, res.data.data.move_area];
            newArrid = [res.data.data.move_province_code, res.data.data.move_city_code, res.data.data.move_area_code];
            service_address = res.data.data.move_address;
            numberPlate = res.data.data.move_numberPlate ? res.data.data.move_numberPlate : '';
            lat = res.data.data.move_lat;
            lng = res.data.data.move_lng;

          }
          that.setData({
            con_signee: res.data.data.contacts,
            mo_bile: res.data.data.contact_number,
            valueName: newArrname,
            getAreaMsg:{
              areaId: newArrid ,
              areaName: newArrname
            },
            add_ress: service_address,
            ch_latitude: lat,
            ch_longitude: lng,
            house_type: res.data.data.contract_info == null ? 0 : res.data.data.contract_info.house_type,
            numberPlate: numberPlate,

          })
        }
      }
    })
  },
  // 定位选择地址
  chooseLocationFun(){
    var that = this;
    wx.chooseLocation({
      success(res){
        that.setData({
          add_ress: res.name,
          ch_latitude: res.latitude,
          ch_longitude: res.longitude,
        })
      }
    })
  },
  //打开地图选择
  openAddress() {
    var that = this;
    this.headid.startAddressAnimation(true);
  },
  //确定接受
  getSure(data) {
    console.log('确定事件获取地址', data);
    var that = this;
    let [province_code, city_code, area_code] = data.detail.areaId;
    let [province, city, area] = data.detail.areaName;
    that.setData({
      getAreaMsg: data.detail
    })
  },

  //联系人输入
  signeeInput: function (e) {
    var that = this;
    var con_signee = e.detail.value;
    that.setData({
      con_signee: con_signee
    })
  },
  //手机号码输入
  bileInput: function (e) {
    var that = this;
    var mo_bile = e.detail.value;
    that.setData({
      mo_bile: mo_bile
    })
  },
  // 输入门牌号
  plateInput(e) {
    var that = this;
    var numberPlate = e.detail.value;
    that.setData({
      numberPlate: numberPlate
    })
  },
  // 选择房屋性质
  radioChange(e) {
    this.setData({
      house_type: e.detail.value
    })
  },
  //判断是修改地址
  joinEdit(e) {
    var that = this;
    var get_e = e;
    //app.saveFormId(e.detail.formId);
    var order_id = that.data.order_id;
    var consignee = e.detail.value.consignee;
    var mobile = e.detail.value.mobile;
    var address = e.detail.value.address + '/' + e.detail.value.numberPlate;
    // var is_default = e.detail.value.is_default==true?2:1; // 是否需要设置成默认地址

    var addr_city = e.detail.value.input;
    var areaName = that.data.getAreaMsg.areaName;
    var areaId = that.data.getAreaMsg.areaId;
    var numreg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/; //手机号码正则  
    var ch_latitude = that.data.ch_latitude;
    var ch_longitude = that.data.ch_longitude;

    if (consignee == '') {
      app.showToast('姓名不能为空', "none", 2000, function () { });
      return false;
    }
    if (mobile == '') {
      app.showToast('手机号码不能为空', "none", 2000, function () { });
      return false;
    }
    if (numreg.test(mobile) == false) {
      app.showToast('请输入正确的手机号码', "none", 2000, function () { });
      return false;
    }
    if (address == false) {
      app.showToast('详细地址信息不能为空', "none", 2000, function () { });
      return false;
    }
    if (areaName.length == 0) {
      app.showToast('请选择省市区信息', "none", 2000, function () { });
      return false;
    }
    app.ajax({
      url: "Engineer/Workorder/editAddr",
      data: {
        work_order_id: order_id,
        province: areaName[0],
        city: areaName[1],
        area: areaName[2],
        province_code: areaId[0],
        city_code: areaId[1],
        area_code: areaId[2],
        service_address: address,
        contacts: consignee,
        contact_number: mobile,
        lat: ch_latitude,
        lng: ch_longitude,
        house_type: that.data.house_type
      },
      success: function (res) {
        if (res.data.code == 1000) {
          app.showToast(res.data.message, "none", 4000, function () { });
          setTimeout(function(){
            wx.navigateBack({
              delta: 1
            })
          },2000)
          
        } else {
          app.showToast(res.data.message, "none", 2000, function () { });
        }
      }
    })

  },
  
})