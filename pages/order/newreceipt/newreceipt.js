// pages/order/newreceipt/newreceipt.js
var util = require("../../../utils/util.js");
var app = getApp();
var getImgarr = []; //需要过度展示图片
var upimgarr = []; //上传七牛云成功后返给后台的图片
var uping = false;
let ne_edCharge = '';//1是需要收取机器费用

Page({

  /**
   * 页面的初始数据
   */
  data: {
    noteMaxLen: 200,
    currentNoteLen: 0,
    imgUrl: [], //选择的照片
    device_model: [],
    device_model_index: '',
    valcontent: '', //内容
    firstClick: true, //第一次点击提交
    armaStorList: [], //配件使用
    basicsStorlist: [], //基础服务
    filterelementList: [], //滤芯服务
    serviceList: [], //服务列表
    allMoney: 0, //总金额
    nums: 0, //总数量
    work_order_id: '',
    go_right: '../../../images/go_right.png',
    startdate: '', //开始日期
    enddate: '', //结束时间
    base_service_name: '', //基础服务名称
    base_service_id: '',//基础服务id
    id_ture: false,
    equipment_type: 0,
    new_device_no: '',
    pressure:'', //水压
    tds:'', //tds
    is_charge:'',//1是需要收取机器费用 0不需要收取机器费用
    charge_money:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    if (options.id != '') {
      that.setData({
        work_order_id: options.id
      })
    }
    getImgarr = [];
    upimgarr = [];
    that.getOrderTypes();
    that.getserviceconfig();//获取安装机型
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    that.getWorkFilter();//获取需要工的
    

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  // 选择安装类型
  deviceModelChange(e){
    var that = this;
    var index = e.detail.value;
    var device_model = that.data.device_model;
    wx.setStorageSync('device_model', { index: index, id: device_model[index].machine_id, name: device_model[index].name});
    that.setData({
      device_model_index: index
    })
  },
  //输入内容字数限制
  charChange: function(e) {
    var value = e.detail.value;
    var len = parseInt(value.length);
    if (len > this.data.noteMaxLen) return;
    wx.setStorageSync('valcontent', value);
    this.setData({
      currentNoteLen: len, //当前字数 ,
      valcontent: value
    });
  },

  // 上传图片
  chooseImg: function() {
    var that = this;
    var num = 3 - getImgarr.length;
    if (uping) {
      return false
    }
    wx.chooseImage({
      count: num, // 默认9  
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有  
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有  
      success: function(res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片 
        var tempFilePaths = res.tempFilePaths
        var url = that.data.imgUrl.concat(tempFilePaths);
        for (var k = 0; k < url.length; k++) {
          that.uplodImg(url[k]); //循环上传图片
        }
      }
    })
  },

  //上传图片到服务器
  uplodImg(chengimg) {
    var that = this;
    wx.showLoading({
      title: '上传图片中',
    })
    uping = true;
    wx.uploadFile({
      url: app.globalData._url + 'Common/Common/upload',
      filePath: chengimg,
      name: 'file',
      success: function(res) {
        wx.hideLoading();
        var data = JSON.parse(res.data);
        if (data.code == 1000) {
          getImgarr.push(data.data.all_url);
          upimgarr.push(data.data.url);
          wx.setStorageSync('showImg', JSON.stringify(getImgarr));
          wx.setStorageSync('upImg', JSON.stringify(upimgarr));
          that.setData({
            imgUrl: getImgarr,
          })
        } else {
          app.showToast("上传头像失败");
        }
      },
      complete: function() {
        uping = false
      }
    })
  },

  //删除图片
  detailImg: function(e) {
    var that = this;
    var index = e.currentTarget.dataset.detail;
    getImgarr.splice(index, 1);
    upimgarr.splice(index, 1);
    if (that.data.imgUrl.length == 0) {
      wx.setStorageSync('showImg', JSON.stringify(getImgarr));
      wx.setStorageSync('upImg', JSON.stringify(upimgarr));
      that.setData({
        imgUrl: getImgarr,
      })
    } else {
      wx.setStorageSync('showImg', JSON.stringify(getImgarr));
      wx.setStorageSync('upImg', JSON.stringify(upimgarr));
      that.setData({
        imgUrl: getImgarr,
      })
    }
  },
  //基础的--减
  bindMinus_A(e) {
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexsn = e.currentTarget.dataset.indexsn;
    // 如果大于1时，才可以减
    if (basicsStorlist[indexsn].goods_num > 1) {
      basicsStorlist[indexsn].goods_num--;
    } else {
      return false;
    }
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num) + parseInt(basicsStorlist[a].other_cost);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num) + parseInt(filterelementList[b].other_cost);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num) + parseInt(armaStorList[c].other_cost);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num) + parseInt(serviceList[d].other_cost);
    }
    //是否需要收机器的钱
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('basics', JSON.stringify(basicsStorlist));
    that.setData({
      basicsStorlist: basicsStorlist,
      allMoney: adMoney / 100,
    });
  },
  bindPlus_A(e) { //加
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexnum = e.currentTarget.dataset.indexnum;
    //自增
    basicsStorlist[indexnum].goods_num++;
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num) + parseInt(basicsStorlist[a].other_cost);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num) + parseInt(filterelementList[b].other_cost);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num) + parseInt(armaStorList[c].other_cost);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num) + parseInt(serviceList[d].other_cost);
    }
    //是否需要收机器的钱

    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('basics', JSON.stringify(basicsStorlist));
    that.setData({
      basicsStorlist: basicsStorlist,
      allMoney: adMoney / 100,
    });
  },
  //滤芯的加减
  bindMinus_B(e) {
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexsn = e.currentTarget.dataset.indexsn;
    // 如果大于1时，才可以减
    if (filterelementList[indexsn].goods_num > 1) {
      filterelementList[indexsn].goods_num--;
    } else {
      return false;
    }
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num);
    }
    //是否需要收机器的钱
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('filterelement', JSON.stringify(filterelementList));
    that.setData({
      filterelementList: filterelementList,
      allMoney: adMoney / 100,
    });
  },
  bindPlus_B(e) { //加
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexnum = e.currentTarget.dataset.indexsn;
    //自增
    filterelementList[indexnum].goods_num++;
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num);
    }
    //是否需要收机器的钱
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('filterelement', JSON.stringify(filterelementList));
    that.setData({
      filterelementList: filterelementList,
      allMoney: adMoney / 100,
    });
  },
  //配件--减
  bindMinus_C: function(e) {
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexnum = e.currentTarget.dataset.indexnum;
    // 如果大于1时，才可以减
    if (armaStorList[indexnum].goods_num > 1) {
      armaStorList[indexnum].goods_num--;
    } else {
      return false;
    }
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num);
    }
    //是否需要收机器的钱
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('armature', JSON.stringify(armaStorList));
    that.setData({
      armaStorList: armaStorList,
      allMoney: adMoney / 100,
    });
  },
  /* 点击加号 */
  bindPlus_C: function(e) {
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexnum = e.currentTarget.dataset.indexnum;
    //自增
    armaStorList[indexnum].goods_num++;
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num);
    }
    //是否需要收机器的钱    
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('armature', JSON.stringify(armaStorList));
    that.setData({
      armaStorList: armaStorList,
      allMoney: adMoney / 100,
    });
  },
  //额外服务 减
  bindMinus_D(e) {
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexsn = e.currentTarget.dataset.indexnum;
    // 如果大于1时，才可以减
    if (serviceList[indexsn].goods_num > 1) {
      serviceList[indexsn].goods_num--;
    } else {
      return false;
    }
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num);
    }
    //是否需要收机器的钱
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('service', JSON.stringify(serviceList));
    that.setData({
      serviceList: serviceList,
      allMoney: adMoney / 100,
    });
  },

  //额外服务 加
  bindPlus_D(e) {
    var that = this;
    var adMoney = 0;
    var filterelementList = that.data.filterelementList;
    var basicsStorlist = that.data.basicsStorlist;
    var armaStorList = that.data.armaStorList;
    var serviceList = that.data.serviceList;
    var indexnum = e.currentTarget.dataset.indexnum;
    //自增
    serviceList[indexnum].goods_num++;
    for (var a in basicsStorlist) {
      adMoney += parseInt(basicsStorlist[a].cost * basicsStorlist[a].goods_num);
    }
    // console.log("a 基础", adMoney);
    for (var b in filterelementList) {
      adMoney += parseInt(filterelementList[b].cost * filterelementList[b].goods_num);
    }
    //console.log("b 滤芯", adMoney);
    for (var c in armaStorList) {
      adMoney += parseInt(armaStorList[c].cost * armaStorList[c].goods_num);
    }
    //console.log("c 其他配件", adMoney);
    for (var d in serviceList) {
      adMoney += parseInt(serviceList[d].cost * serviceList[d].goods_num);
    }
      //是否需要收机器的钱
    if (ne_edCharge.is_collect_money == 1 && ne_edCharge != '') {
      adMoney += parseInt(ne_edCharge.money * 100)
    }
    // 将数值与状态写回  顺带缓存
    wx.setStorageSync('service', JSON.stringify(serviceList));
    that.setData({
      serviceList: serviceList,
      allMoney: adMoney / 100,
    });
  },

  //基础服务
  basicsfun() {
    var that = this,
      work_order_id = that.data.work_order_id;
    wx.navigateTo({
      url: '../basics/basics?work_order_id=' + work_order_id,
    })
  },

  //滤芯服务
  filterelement() {
    var that = this,
      work_order_id = that.data.work_order_id;
    wx.navigateTo({
      url: '../filterelement/filterelement?work_order_id=' + work_order_id,
    })
  },

  //其他配件选择
  armature() {
    var that = this,
      work_order_id = that.data.work_order_id;
    wx.navigateTo({
      url: '../armature/armature?work_order_id=' + work_order_id,
    })
  },

  //额外服务
  additional() {
    var that = this,
      work_order_id = that.data.work_order_id;
    wx.navigateTo({
      url: '../additional/additional?work_order_id=' + work_order_id,
    })
  },
  //去下单
  goNext() {
    var that = this,
      work_order_id = that.data.work_order_id,
      valcontent = that.data.valcontent,
      startdate = that.data.id_ture ? that.data.startdate : '',
      enddate = that.data.id_ture ? that.data.enddate : '';
    var basicsStor = wx.getStorageSync('basics') ? JSON.parse(wx.getStorageSync('basics')) : [];
    if (basicsStor.length <= 0) {
      app.showToast('至少选择一项基础服务', "none", 3000, function() {});
      return false;
    }
    if (that.data.device_model_index == '' && that.data.base_service_id.indexOf('3')!=-1){
      app.showToast('请选择安装机型');
      return false;
    }
    // if (startdate == '' || enddate=='') {
    //   app.showToast('确认保修时间不能为空', "none", 3000, function () { });
    //   return false;
    // }
    wx.navigateTo({
      url: '../notarize/notarize?work_order_id=' + work_order_id + '&startdate=' + startdate + '&enddate=' + enddate + "&equipment_type=" + that.data.equipment_type + "&new_device_no=" + that.data.new_device_no + "&pressure=" + that.data.pressure + "&tds=" + that.data.tds,
    })
  },
  // 选择日期
  bindStartDateChange: function(e) {
    //console.log(e.detail.value);
    var that = this;
    var startdate = e.detail.value;
    that.setData({
      startdate: startdate
    })
  },
  // 选择时间
  bindStartTimeChange: function(e) {
    //console.log(e.detail.value);
    var that = this;
    var starttime = e.detail.value;
    that.setData({
      starttime: starttime
    })

  },
  // 截止时间
  // 选择日期
  bindEndDateChange: function(e) {
    //console.log(e.detail.value);
    var that = this;
    var enddate = e.detail.value;
    that.setData({
      enddate: enddate
    })

  },
  // 选择时间
  bindEndTimeChange: function(e) {
    // console.log(e.detail.value);
    var that = this;
    var endtime = e.detail.value;
    that.setData({
      endtime: endtime
    })

  },
  //获取订单保修时间以及订单类型....
  getOrderTypes() {
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/getServiceInfo',
      data: {
        work_order_id: that.data.work_order_id,
      },
      success: function(res) {
        var getData = res.data.data;
        if (res.data.code == 1000) {
          // app.showToast(res.data.message, "none", 2000, function () {});
         
          that.setData({
            startdate: getData.info.start_date,
            enddate: getData.info.end_date,
            equipment_type: getData.info.equipment_type
          })
          var base_service_arr = getData.base_service_arr;
          var base_service_id = getData.base_service_id; //id中
          var idture = false;
          var basicsStor = wx.getStorageSync('basics') ? JSON.parse(wx.getStorageSync('basics')) : [];
          var base_kong = []; //基础
          var getMoney = 0;
          var nums = 0;
          if (base_service_arr.length > 0 && basicsStor.length < 1) {
            // console.log('需要修改的数组', base_service_arr);
            for (var n in base_service_arr) {
              var objb = {};
              objb.name = base_service_arr[n].name;
              objb.business_type_id = base_service_arr[n].business_id;
              objb.goods_num = base_service_arr[n].num; // cost_unit
              objb.cost_unit = base_service_arr[n].cost_unit;
              objb.icon = base_service_arr[n].icon;
              objb.cost = base_service_arr[n].cost;
              objb.other_cost = base_service_arr[n].other_cost;
              objb.other_cost_unit = base_service_arr[n].other_cost_unit;
              base_kong.push(objb);
              getMoney += parseInt(base_service_arr[n].cost) * parseInt(base_service_arr[n].num) + base_service_arr[n].other_cost;
              //console.log('数量', base_service_arr[n].money,'数量+++++', parseInt(base_service_arr[n].num));
            }
            for (var s in base_service_id) {
              if (base_service_id[s] == 3) {
                idture = true
              }
            }
            //console.log('真假', getData.new_install.is_collect_money);
            wx.setStorageSync('basics', JSON.stringify(base_kong));
            wx.setStorageSync('need_charge', getData.new_install)
            if (getData.new_install.is_collect_money==1){
              that.setData({
                basicsStorlist: base_kong,
                allMoney: (parseInt(getMoney) + parseInt(getData.new_install.money * 100)) / 100,
                nums: base_kong.length,
                base_service_name: getData.base_service_name,
                base_service_id: base_service_id,
                is_charge: getData.new_install.is_collect_money,
                charge_money: getData.new_install.money,
                id_ture: idture
              });
            }else{
              that.setData({
                basicsStorlist: base_kong,
                allMoney: getMoney / 100,
                nums: base_kong.length,
                base_service_name: getData.base_service_name,
                base_service_id: base_service_id,
                is_charge: getData.new_install.is_collect_money,
                charge_money: getData.new_install.money,
                id_ture: idture
              });
            }
          

          }
        } else {
          app.showToast(res.data.message);
        }
      }
    })

  },
  //更换主板
  changeBord() {
    var that = this;
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/changebord/changebord?id=' + that.data.work_order_id,
    })
  },

  // 获取水压
  getpressure(e){
    this.setData({
      pressure: e.detail.value
    })    
  },

  // 获取tds
  gettds(e){
    this.setData({
      tds: e.detail.value
    }) 
  },
  //获取安装机型
  getserviceconfig() {
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/getMachineList',
      data: {
        key: 'device_model',
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            device_model: res.data.data,
          })
        } else {
          app.showToast(res.data.message);
        }
        
      }
    })

  },
  //计算价格
  getPriceFun(){
    console.log('计算价格')
    var that = this;
    var getMoney = 0;
    var nums = 0;
    var armaStor = wx.getStorageSync('armature') ? JSON.parse(wx.getStorageSync('armature')) : [];
    var basicsStor = wx.getStorageSync('basics') ? JSON.parse(wx.getStorageSync('basics')) : [];
    var filterelement = wx.getStorageSync('filterelement') ? JSON.parse(wx.getStorageSync('filterelement')) : [];
    var service = wx.getStorageSync('service') ? JSON.parse(wx.getStorageSync('service')) : [];
    var valcontent = wx.getStorageSync('valcontent') ? wx.getStorageSync('valcontent') : '';
    var showImg = wx.getStorageSync('showImg') ? JSON.parse(wx.getStorageSync('showImg')) : [];
    var upImg = wx.getStorageSync('upImg') ? JSON.parse(wx.getStorageSync('upImg')) : [];
    var device_model = wx.getStorageSync('device_model') ? wx.getStorageSync('device_model') : '';
    var needCharge = wx.getStorageSync('need_charge') ? wx.getStorageSync('need_charge') : '';
    var base_name = '';

    getImgarr = showImg;
    upimgarr = upImg;
    console.log('>><配件', armaStor);
    console.log('>><基础服务>>', basicsStor);
    // console.log('>><滤芯', filterelement);
    //console.log('>><额外', service);
    nums = parseInt(armaStor.length) + parseInt(basicsStor.length) + parseInt(filterelement.length) + parseInt(service.length);

    for (var a in basicsStor) {
      getMoney += parseInt(basicsStor[a].cost * basicsStor[a].goods_num) + parseInt(basicsStor[a].other_cost);
      base_name += basicsStor[a].name + " "
    }
    for (var b in filterelement) {
      getMoney += parseInt(filterelement[b].cost * filterelement[b].goods_num);
    }
    for (var c in armaStor) {
      getMoney += parseInt(armaStor[c].cost * armaStor[c].goods_num);
    }
    for (var d in service) {
      getMoney += parseInt(service[d].cost) * parseInt(service[d].goods_num);
    }
    //是否需要收机器的钱
    if (needCharge.is_collect_money == 1 && needCharge != '') {
      getMoney += parseInt(needCharge.money * 100)
    }
    ne_edCharge = needCharge;
    that.setData({
      armaStorList: armaStor,
      basicsStorlist: basicsStor,
      filterelementList: filterelement,
      serviceList: service,
      allMoney: getMoney / 100,
      nums: nums,
      valcontent: valcontent,
      currentNoteLen: valcontent.length,
      imgUrl: showImg,
      base_service_name: base_name,
      device_model_index: device_model.index || ''
    })
    if (util.isExitsVariable(wx.getStorageSync('new_device_no'))) {
      that.setData({
        new_device_no: wx.getStorageSync('new_device_no')
      })
    }
    // if (util.isExitsVariable(wx.getStorageSync('new_device_no_' + that.data.work_order_id))) {
    //   that.setData({
    //     new_device_no: wx.getStorageSync('new_device_no_' + that.data.work_order_id)
    //   })
    // }
  },
  //获取工单需要换的芯
  getWorkFilter: function () {
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/getWorkOrderParts',
      data: {
        work_order_id: that.data.work_order_id,
        is_filter: 2,
      },
      success: function (res) {
        if (res.data.code == 1000) {

          console.log(res.data.data.length>0);
        }
        //判断工单中是否有需要换芯的滤芯 [有就存入缓存(需要在所有滤芯中循环出来)] 并且保证没有缓存()缓存优先
        var filterelement = wx.getStorageSync('filterelement') ? JSON.parse(wx.getStorageSync('filterelement')) : [];
        console.log('第一步', filterelement.length);
        if (res.data.data.length > 0 && filterelement.length<=0){
          that.getPartsList(res.data.data);
          console.log('进来1')
        }else{
          that.getPriceFun();//计算价格数量
        }
        
      }
    })
  },
  // 获取配件列表
  getPartsList(allPartList) {
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/getEquipPartsList',
      data: {
        work_order_id: that.data.work_order_id,
        keywords: '',
        page: '-1',
        is_filter: 1
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var partsList = res.data.data;
          for (var i in partsList) {
            partsList[i].goods_num = 1;
          }
          //回显-若已有缓存取缓存,反之取工单设置的滤芯值
          var armaStor = allPartList;
          var getNewArr=[];
            if (armaStor.length > 0) {
              for (var c in armaStor) {
                for (var i in partsList) {
                  if (armaStor[c].parts_id == partsList[i].parts_id) {
                    partsList[i].cycle = armaStor[c].cycle;
                    partsList[i].goods_num = typeof (armaStor[c].goods_num) == "undefined" ? 1 : armaStor[c].goods_num;
                    getNewArr.push(partsList[i]);
                    break;
                  }
                }
              }
            }
          console.log('这才是?', getNewArr);
          // console.log('这是我想的数据?', partsList);
          // return false;
          wx.setStorageSync('filterelement', JSON.stringify(getNewArr));
          that.getPriceFun();//计算价格数量
        } 
      }
    })
  },
})