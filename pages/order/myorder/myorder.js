// pages/order/myorder/myorder.js
var timer; // 计时器
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
var flowDoor = app.globalData._network_path + 'flowDoor.jpg';
let load = true, tabNum = 8, lat1 = '', lng1 = '', startX = '', startY='';
//                          当前位置
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone_icon: '../../../images/phone_icon.png',
    select_bottom: '../../../images/select_bottom.png',
    tabs: [{
        tab_id: 0,
        tab_name: '待处理',
        tab_icon: '../../../images/pending_h.png',
        tab_icon_active: '../../../images/pending_b.png',
        tab_num: 8,
      },
      {
        tab_id: 1,
        tab_name: '已预约',
        tab_icon: '../../../images/appointment_h.png',
        tab_icon_active: '../../../images/appointment_b.png',
        tab_num: 9,
      },
      {
        tab_id: 2,
        tab_name: '处理中',
        tab_icon: '../../../images/deiling_h.png',
        tab_icon_active: '../../../images/deiling_b.png',
        tab_num: 10,
      },
      {
        tab_id: 3,
        tab_name: '已完成',
        tab_icon: '../../../images/evaluation_h.png',
        tab_icon_active: '../../../images/evaluation_b.png',
        tab_num: 11,
      }
    ],
    tabIndex: 0,
    reasonData: [],
    reasonIndex: "",
    placeholder: '请说明退单原因~',
    reasonShow: true,
    chargebackId: '', //退还工单号
    chargebackNum: '', //退还工单编号
    remarks: '', //退还备注
    tipShow: true, //退还订单显示
    dropShow: true, //上门显示
    orderData: [],
    work_order_id: '', //上门工单ID
    empty: true,
    hasMore: false,
    loading: false,
    orderNumbers: [0, 0, 0, 0], //订单数量
    loadShow: false,
    flowDoor: flowDoor,
    callShow: false,//拨打电话弹窗显示
    callItem: '',// 点击拨打的工单详情
    callIndex: 0,//选择联系人|紧急联系人

    searchKey: '', //搜索关键字
    ordertypeList: [], //订单类型列表
    selectTypeid: [], //选中的订单类型ID
    distanceList: [], //距离列表
    selectDistance: '', //选中的距离
    isMaskShow: false,//遮罩层显隐
    move: 0,//筛选弹窗
    screenShow: false,//筛选模块显隐,
    // delBtnWidth: 160,
    isSlide: wx.getStorageSync('isSlide')||false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.getBussinessType();
    that.getDistanceList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    app.loginStatus(function() {}, function() {})
    var tabIndex = wx.getStorageSync('tabIndex') || 0;
    that.setData({
      tabIndex: tabIndex,
      empty: true,
      hasMore: false,
      loading: false,
    });
    load = true;
    tabNum = that.data.tabs[tabIndex].tab_num;
    _PAGE = 1;
    that.getOrderList();
    that.getReturnReason();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    var that = this;
    that.setData({
      loadShow: false
    })

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    wx.setStorageSync('tabIndex', 0);

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var that = this;
    _PAGE = 1;
    this.getOrderList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getOrderList();
  },
  // 获取订单列表与数量
  getOrderList: function() {
    var that = this;
    app.ajax({
      url: 'Engineer/Order/getRepairOrder',
      load: load,
      msg: '数据加载中...',
      data: {
        work_order_status: tabNum,
        business_type: that.data.selectTypeid,
        distance_range: that.data.selectDistance,
        searchKey: that.data.searchKey,
        page: _PAGE,
        pagesize: _PAGESIZE
      },
      success: function(res) {
        wx.stopPullDownRefresh();
        that.setData({
          orderNumbers: [res.data.data.work_num_8, res.data.data.work_num_9, res.data.data.work_num_10, res.data.data.work_num_11]
        });
        if (res.data.code == 1000) {
          if (_PAGE == 1) {
            that.setData({
              orderData: res.data.data.work_list,
              empty: false
            });
          } else {
            that.setData({
              orderData: that.data.orderData.concat(res.data.data.work_list),
              empty: false
            });
          }
          for (var index in that.data.orderData) {
            var isTouchMove = "orderData[" + index + "].isTouchMove";
            that.setData({
              [isTouchMove]: false //默认隐藏删除
            })
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.work_list.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    })


  },
  //切换栏切换
  tabChange: function(e) {
    let that = this;
    let index = e.currentTarget.dataset.index;
    let name = e.currentTarget.dataset.name;
    wx.setStorageSync('tabIndex', index);
    that.setData({
      tabIndex: index,
      empty: true,
      hasMore: false,
      loading: false,
      loadShow: false,
      searchKey:'',
      selectDistance:'',
      selectTypeid: []
    });
    load = true;
    tabNum = that.data.tabs[index].tab_num;
    _PAGE = 1;
    that.getOrderList();
  },
  // 打开拨打电话弹窗
  openCall(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    console.log('index',index);
    var orderData = that.data.orderData;
    var item = orderData[index];
    console.log('item',item)
    that.setData({
      callShow: true,
      callItem: item,
      callIndex: 0
    })
  },
  // 关闭拨打电话弹窗
  cancelCall(){
    var that = this;
    that.setData({
      callShow: false,
      callItem: false
    })
  },
  // 跳转紧急联系人
  goEmergency(){
    var that = this;
    var callItem = that.data.callItem;
    wx.navigateTo({
      url: '../emergencyContact/emergencyContact?user_id=' + callItem.user_id + '&work_order_id=' + callItem.work_order_id + '&emergency_contact=' + JSON.stringify(callItem.emergency_contact),
    })
    that.cancelCall();
  },
  // 切换选择联系人|紧急联系人
  callChoose(e){
    var that = this;
    var index = e.currentTarget.dataset.index;
    that.setData({
      callIndex: index
    })
  },
  // 确认选中拨打电话
  sureTelphone(){
    var that = this;
    var index = that.data.callIndex;
    var callItem = that.data.callItem;
    var number = callItem.contact_number;
    if(index == 1){
      number = callItem.emergency_contact.tel
    }
    wx.makePhoneCall({
      phoneNumber: number,
    })
    that.cancelCall();
  },
  //联系他
  telPhone: function(e) {
    var that = this;
    let phone = e.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: phone,
    })
    that.cancelCall();
  },
  //获取退单原因列表
  getReturnReason: function() {
    var that = this;
    app.ajax({
      url: 'Common/Common/getReturnReason',
      data: {},
      success: function(res) {
        var data = res.data.data;
        var reasonData = [];
        for (var i in data) {
          reasonData.push({
            id: i,
            content: data[i]
          });

        }
        that.setData({
          reasonData: reasonData,
        })

      }
    })
  },
  // 激活设备
  activation:function(e){
    var that = this;
    var work_order_id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../activation/activation?work_order_id=' + work_order_id,
    })
  },
  //退还订单
  chargeback: function(e) {
    let that = this;
    let reasonShow = false;
    let id = e.currentTarget.dataset.id;
    let orderid = e.currentTarget.dataset.orderid;
    that.setData({
      reasonShow: reasonShow,
      chargebackId: id,
      chargebackNum: orderid
    })
  },
  //退还原因
  bindPickerChange: function(e) {
    let that = this;
    let reasonIndex = e.detail.value;
    that.setData({
      reasonIndex: reasonIndex
    })
  },
  //退还备注
  inputRemarks: function(e) {
    var that = this;
    var remarks = e.detail.value;
    that.setData({
      remarks: remarks
    });
  },
  //确定退还
  confirm: function(e) {
    let that = this;
    let reasonShow = true;
    let tipShow = false;
//    app.saveFormId(e.detail.formId);
    var id = that.data.chargebackId;
    var reasonIndex = that.data.reasonIndex;
    var remarks = that.data.remarks;
    var reason_id = reasonIndex==''?'':that.data.reasonData[reasonIndex].id;
    console.log(reason_id);
    if (reasonIndex == '' || remarks == '') {
      app.showToast("请填写完整信息", "none", 2000, function() {});
      return;
    }
    app.ajax({
      url: 'Engineer/Workorder/backOrderApply',
      data: {
        work_order_id: id,
        reason_id: reason_id,
        remark: remarks
      },
      success: function(res) {
        if (res.data.code == 1000) {
          that.setData({
            reasonShow: reasonShow,
            tipShow: tipShow,
            remarks: '',
            reasonIndex: ''
          })
          _PAGE = 1;
          timer = setTimeout(function() {
            let tipShow = true;
            that.setData({
              tipShow: tipShow
            })
            that.getOrderList();
          }, 2000);
        } else {
          app.showToast(res.data.message, "none", 2000, function() {});
        }

      }
    })

  },
  //取消退还
  cancel: function() {
    let that = this;
    let reasonShow = true;
    that.setData({
      reasonShow: reasonShow,
      remarks: '',
      reasonIndex: ''
    })
  },
  //更换产品
  changeProduct: function(e){
    var work_order_id = e.currentTarget.dataset.id;
    app.ajax({
      url: 'Engineer/Equipments/isBind',
      data: {
        work_order_id: work_order_id
      },
      success: function (res) {
        if (res.data.code == 1000) {
          if (res.data.data.is_bind == 0) {
            wx.navigateTo({
              url: '../changeProduct/changeProduct?work_order_id=' + work_order_id,
            })
          } else {
            wx.navigateTo({
              url: '../../myCenter/deviceReplace/deviceReplace?work_order_id=' + work_order_id,
            })
          }
        }
      }
    })
    
    
  },
  //确认预约时间
  amendTime: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../amendTime/amendTime?id=' + id,
    })
  },
  //上门
  dropIn: function(e) {
    let that = this;
    let dropShow = false;
    var id = e.currentTarget.dataset.id;
    wx.getLocation({
      // type: 'wgs84',
      type: 'gcj02',
      success: function(res) {
        lat1 = res.latitude;
        lng1 = res.longitude;
        console.log(lat1, lng1, res.accuracy);
        that.setData({
          dropShow: dropShow,
          work_order_id: id,
        })
      }
    });


  },
  //确定上门
  dropConfirm: function(e) {
    let that = this;
    let dropShow = true;
    var id = that.data.work_order_id;
   // app.saveFormId(e.detail.formId);
    app.ajax({
      url: 'Engineer/Workorder/goHome',
      data: {
        work_order_id: id,
        lat: lat1,
        lng: lng1
      },
      success: function(res) {
        if (res.data.code == 1000) {
          app.showToast("确认成功", "none", 2000, function() {});
          _PAGE = 1;

          wx.setStorageSync('tabIndex', 2);
          that.setData({
            tabIndex: 2,
          })
          tabNum = that.data.tabs[2].tab_num;
          that.getOrderList();
        } else {
          app.showToast(res.data.message, "none", 2000, function() {});
        }
      }
    })

    that.setData({
      dropShow: dropShow
    })
  },
  //取消上门
  dropCancel: function() {
    let that = this;
    let dropShow = true;
    that.setData({
      dropShow: dropShow
    })
  },
  //导航
  orderMap: function(e) {
    console.log(e)
    var lat = parseFloat(e.currentTarget.dataset.lat)
    var lng = parseFloat(e.currentTarget.dataset.lng)
    
    wx.openLocation({
      latitude: lat,
      longitude: lng,
      scale: 28
    })
  },
  // 确认安装地址
  confirmAddress(e){
    var that = this;
    var order_id = e.currentTarget.dataset.id;
    var type = e.currentTarget.dataset.type;
    if(type != 2){
      type = 1;
    }
    wx.navigateTo({
      url: '../confirmAddress/confirmAddress?order_id=' + order_id +'&type='+type,
    })
  },
  // 绑定设备
  bindEquipment: function(e) {
    var id = e.currentTarget.dataset.id;
    var user_id = e.currentTarget.dataset.user_id;
    wx.navigateTo({
      url: '../../order/import/import?id=' + id+"&user_id="+user_id,
    })
  },
  // 确认完成
  finishOn: function(e) {
    var that = this;
    wx.removeStorageSync('basics'); //清除缓存
    wx.removeStorageSync('filterelement'); //清除缓存
    wx.removeStorageSync('service'); //清除缓存
    wx.removeStorageSync('valcontent'); //清除缓存
    wx.removeStorageSync('showImg'); //清除缓存
    wx.removeStorageSync('upImg'); //清除缓存  
    wx.removeStorageSync('armature'); //清除缓存
    wx.removeStorageSync('new_device_no');//清除缓存
    wx.removeStorageSync('device_model');//清除缓存
    wx.removeStorageSync('need_charge');
    wx.removeStorageSync('changemoney');//清楚可修改金额

    var finishId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../newreceipt/newreceipt?id=' + finishId,
    })
  },
  //查看详情
  orderDetail: function(e) {
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../orderDetail/orderDetail?id=' + id,
    })
  },
 
  //签合同
  sign: function(e) {
    wx.navigateTo({
      url: '../../myCenter/econtract/econtract?contract_id=' + e.currentTarget.dataset.contract_id + "&user_id=" + e.currentTarget.dataset.user_id + "&work_order_id=" + e.currentTarget.dataset.id,
    })
  },
  // 阻止遮罩滚动
  myCatchTouch: function() {
    return;

  },
  //跳转我的认证
  authentication(e) {
    var that = this;
    var user_id = e.currentTarget.dataset.user_id;
    app.ajax({
      url: "Engineer/Realname/getAuthInfo",
      data: { user_id: user_id },
      success: function (res) {
        if (res.data.code == 1000 && (res.data.data.status == -2 || res.data.data.status == -4)) {
          wx.navigateTo({
            url: '../../realname/uploadDocuments/uploadDocuments?user_id=' + user_id,
          })
        } else if (res.data.code == 1000 && res.data.data.status == -3) {
          wx.navigateTo({
            url: '../../realname/faceRecognition/faceRecognition?user_id=' + user_id,
          })
        } else if (res.data.code == 1000 && (res.data.data.status == 2 || res.data.data.status == 1)) {
          wx.navigateTo({
            url: '../../realname/myCertification/myCertification?user_id=' + user_id,
          })
        } else {
          wx.navigateTo({
            url: '../../realname/realName/realName?user_id=' + user_id,
          })
        }
      }

    })
  },
  // 查看流程大图
  viewPicture(e){
    var url = e.currentTarget.dataset.url;
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },

  
  // 输入搜索字段
  inputSearch: function (e) {
    var that = this;
    var searchKey = e.detail.value;
    that.setData({
      searchKey: searchKey
    });
  },
  //小键盘直接搜索
  searchOrder: function (e) {
    var that = this;
    var key = e.detail.value;
    // if (key == '' || key == null) {
    //   app.showToast("请输入搜索关键字", "none", 2000, function () { });
    //   return;
    // }
    that.setData({
      orderData: [],
      searchKey: key
    })
    _PAGE = 1;
    that.getOrderList();
  },
  //点击搜索图标搜索
  searchOrder2: function () {
    var that = this;
    var key = that.data.searchKey;
    // if (key == '' || key == null) {
    //   app.showToast("请输入搜索关键字", "none", 2000, function () { });
    //   return;
    // }
    that.setData({
      orderData: [],
    })
    _PAGE = 1;
    that.getOrderList();
  },
  // 获取订单类型列表
  getBussinessType: function () {
    var that = this;
    app.ajax({
      url: 'Common/Common/getBussinessType',
      data: {},
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            screenTypes: res.data.data
          })
        } else {
          // 没有数据
        }
      }
    })
  },
  // 获取距离列表
  getDistanceList: function () {
    var that = this;
    app.ajax({
      url: 'Common/Common/getServiceConfig',
      data: {
        key: 'distance'
      },
      success: function (res) {
        if (res.data.code == 1000) {
          that.setData({
            distanceList: res.data.data
          })
        } else {
          // 没有数据
        }
      }
    })
  },
  // 筛选模块显、隐
  chooseType: function () {
    var that = this;
    if (that.data.screenShow == false) {
      that.setData({
        screenShow: true,
        isMaskShow: true,
        move: 0
      })
    } else {
      that.setData({
        move: 1,
      })
      setTimeout(function () {
        that.setData({
          screenShow: false,
          isMaskShow: false,
        })
      }, 500)
    }
  },
  // 关闭弹窗
  closeMask() {
    var that = this;
    that.setData({
      move: 1,
    })
    setTimeout(function () {
      that.setData({
        screenShow: false,
        isMaskShow: false,
      })
    }, 500)
  },

  //选择优惠活动分类
  ordertypeChoose(e) {
    var that = this;
    var obj = that.data.selectTypeid;
    var id = e.currentTarget.dataset.id;
    if (that.data.selectTypeid.indexOf(id) < 0) {
      obj.push(id);
    } else {
      obj.splice(that.data.selectTypeid.indexOf(id), 1)
    }
    console.log(obj);
    that.setData({
      selectTypeid: obj
    })
  },
  //选择距离分类
  distanceChoose(e) {
    var that = this;
    that.setData({
      selectDistance: e.currentTarget.dataset.id
    })
  },
  //确定选择的活动和距离
  sureChoose() {
    var that = this;
    that.setData({
      orderData: [],
    })
    that.closeMask();
    _PAGE = 1;
    that.getOrderList();
  },
  //重置
  reset() {
    var that = this;
    that.setData({
      orderData: [],
      selectTypeid: [],
      selectDistance: '',
    })
    _PAGE = 1;
    that.getOrderList();
  },
  //添加到日程
  addDateOrder(e){
    wx.navigateTo({
      url: '/pages/subpackage/pages/order/editTime/editTime?work_order_id=' + e.currentTarget.dataset.id + '&open_type=' + 1
    })
  },
  //企业用户签合同
  signTip(){
    app.showToast("该用户非个人客户,请告知客户登录用户端小程序进行签署!")
  },


  angle: function (start, end) {
    var _X = end.X - start.X,
      _Y = end.Y - start.Y
    //返回角度 /Math.atan()返回数字的反正切值
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI);
  },
  //手指触摸动作开始 记录起点X坐标
  touchstart: function (e) {
    if (e.currentTarget.dataset.status!=9) return false;
    //开始触摸时 重置所有删除
    this.data.orderData.forEach(function (v, i) {
      if (v.isTouchMove)//只操作为true的
        v.isTouchMove = false;
    })
    startX = e.changedTouches[0].clientX;
    startY = e.changedTouches[0].clientY;
    this.setData({
      orderData: this.data.orderData
    })

  },
  //滑动事件处理
  touchmove: function (e) {
    if (e.currentTarget.dataset.status != 9) return false;
    var that = this,
      index = e.currentTarget.dataset.index,//当前索引
      touchMoveX = e.changedTouches[0].clientX,//滑动变化坐标
      touchMoveY = e.changedTouches[0].clientY,//滑动变化坐标
      //获取滑动角度
      angle = that.angle({ X: startX, Y: startY }, { X: touchMoveX, Y: touchMoveY });
    that.data.orderData.forEach(function (v, i) {
      v.isTouchMove = false
      //滑动超过30度角 return
      if (Math.abs(angle) > 30) return;
      if (i == index) {
        if (touchMoveX > startX) //右滑
          v.isTouchMove = false
        else //左滑
          v.isTouchMove = true;
          setTimeout(()=>{
            that.setData({
              isSlide: true
            })
            wx.setStorageSync('isSlide', true);
          },1500)
          
      }
    })
    //更新数据
    that.setData({
      orderData: that.data.orderData
    })
  },

})