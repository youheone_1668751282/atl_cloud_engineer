// pages/import/import.js
var util = require("../../../utils/util.js"); 
var app = getApp();
var moreup=false;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    values:'',
    work_order_id: '',
    user_id:'',
    scanning:'../../../images/scanning.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var id = options.id;
    that.setData({
      work_order_id: id,
      user_id:options.user_id
    })
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    moreup = false;
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  // 跳转设备调试
  goDebug(){
    wx.navigateTo({
      url: '/pages/subpackage/pages/device/test/test',
    })
  },
  // 跳转提示页面
  goDebugTips(){
    wx.navigateTo({
      url: '../../debugTips/debugTips',
    })
  },
  //input 输入框
  inputfun(e){
    var that=this;
    that.setData({
      values: e.detail.value
    })
  },
  // 扫码
  scanning: function(){
    var that = this;
    app.scanning(function (data) {
      that.setData({
        values: data
      })
    })
    // wx.scanCode({
    //   success: (res) => {
        
    //     console.log(res)
    //     var code = res.result.split(';')
    //     if (!util.isExitsVariable(code)||code.length==0){
    //       app.showToast('扫码失败');
    //       return false;
    //     }
    //     var val = code[2].split(':');
    //     if (!util.isExitsVariable(val)||val.length==0) {
    //       app.showToast('扫码失败');
    //       return false;
    //     }
    //     that.setData({
    //       values: val[1]
    //     })

    //   },
    //   fail: (res) => {
    //     app.showToast('扫码失败，请重试！');
    //   }
    // })
  },
  //确认事件 设备信息
  affirmfun(e){
    var that = this;
    if(that.data.values == '' || that.data.values == null){
      app.showToast("请输入设备编号","none",2000,function(){});
      return;
    }
    if(moreup){
      console.log('??', moreup)
      return false
    }
    moreup=true;
    var URL = 'Engineer/Workorder/bindEquipment';
    var work_order_id = that.data.work_order_id;
    var device_no = that.data.values;
    var token = wx.getStorageSync('token');
//    app.saveFormId(e.detail.formId);
    app.ajax({
      url: URL,
      data:{
        work_order_id: work_order_id,
        device_no: device_no
      },
      header:{
        'token': token
      },
      success: function(res){
        if(res.data.code == 1000){
          app.showToast("绑定成功","none",2000,function(){});
          var timer;
          timer = setTimeout(function () {
            moreup=false
            wx.navigateBack({
              delta: 1
            })
          }, 1000);
        }else{
          moreup=false
          app.showToast(res.data.message, "none", 2000, function (){});             if(res.data.code==-1201){//如果未进行实名跳转到实名页面
            that.toRealname();
          }
        }
       
      }
    })

  },
  //跳转到实名
  toRealname(){
    var that = this;
      app.ajax({
        url: "Engineer/Realname/getAuthInfo",
        data: { user_id: that.data.user_id },
        success: function (res) {
          if (res.data.code == 1000 && res.data.data.status == -2) {
            wx.navigateTo({
              url: '../../realname/uploadDocuments/uploadDocuments?user_id=' + that.data.user_id,
            })
          } else if (res.data.code == 1000 && res.data.data.status == -3) {
            wx.navigateTo({
              url: '../../realname/faceRecognition/faceRecognition?user_id=' + that.data.user_id,
            })
          } else if (res.data.code == 1000 && (res.data.data.status == 2 || res.data.data.status == 1)) {
            wx.navigateTo({
              url: '../../realname/myCertification/myCertification?user_id=' + that.data.user_id,
            })
          } else {
            wx.navigateTo({
              url: '../../realname/realName/realName?user_id=' + that.data.user_id,
            })
          }
        }


      })
    
  }
  
})