// pages/order/armature/armature.js
var app = getApp();
var _PAGE = 1;
var _PAGESIZE = 10;
let load = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    search_type: 0,//搜索
    operation_type: 0, //是否编辑
    search_iocn: '../../../images/search_iocn.png',
    partsList: [],//配件数据
    checkedAll: false, //全选
    search_name: "",//搜索条件
    empty: true,
    hasMore: false,
    loading: false,
    loadShow: false,
    work_order_id:'',//工单ID
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.work_order_id != '') {
      that.setData({
        work_order_id: options.work_order_id||1234
      })
    } else {
      app.showToast("工单号为空请重试", "none", 2000, function () { });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    that.setData({
      search_name: '',
    });
    _PAGE = 1;
    load = true;
    var search_name = that.data.search_name;
    that.getPartsList(search_name);

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var search_name = that.data.search_name;
    var hasMore = that.data.hasMore;
    if (!hasMore) {
      return false;
    }
    _PAGE++;
    that.setData({
      loading: true
    })
    that.getPartsList(search_name);
  },
  // 获取配件列表
  getPartsList: function (key) {
    var that = this;
    app.ajax({
      url: 'Engineer/Workorder/partsList',
      load: load,
      msg: '配件加载中...',
      data: {
        work_order_id: that.data.work_order_id,
        keywords: key,
        page: _PAGE,
        pageSize: _PAGESIZE
      },
      success: function (res) {
        if (res.data.code == 1000) {
          var partsList = res.data.data;
          for (var i in partsList) {
            partsList[i].checked = false;
            partsList[i].goods_num = 1;
          }
          //回显
          var armaStor = wx.getStorageSync('armature') ? JSON.parse(wx.getStorageSync('armature')) : [];
          var checkedAll=false;

          //单页的情况
          if (_PAGE == 1) {

            if (armaStor.length > 0) {
              for (var c in armaStor) {
                for (var i in partsList) {
                  if (armaStor[c].parts_id == partsList[i].parts_id) {
                    partsList[i].checked = true;
                    partsList[i].goods_num = armaStor[c].goods_num;
                  }
                }
              }
            }
            if (armaStor.length == partsList.length){
              checkedAll=true;
            }
            that.setData({
              partsList: partsList,
              empty: false,
              checkedAll: checkedAll
            });

          } else {
            //分页的情况
            // 回选是否全选....
            var getconcatList = that.data.partsList.concat(partsList);
            if (armaStor.length == getconcatList.length) {
              checkedAll = true;
            }
            if (armaStor.length > 0) {
              for (var c in armaStor) {
                for (var i in getconcatList) {
                  if (armaStor[c].parts_id == getconcatList[i].parts_id) {
                    getconcatList[i].checked = true;
                    getconcatList[i].goods_num = armaStor[c].goods_num;
                  }
                }
              }
            }
            that.setData({
              partsList: getconcatList,
              empty: false,
              checkedAll: checkedAll
            });
          }
          // 是否加载更多
          var hasMore = true;
          if (res.data.data.length < _PAGESIZE) {
            hasMore = false;
          }
          that.setData({
            hasMore: hasMore,
            loading: false
          });
          load = false;
        } else {
          if (_PAGE == 1) {
            that.setData({
              hasMore: false,
              loading: false,
              empty: true
            })
          } else {
            that.setData({
              hasMore: false,
              loading: false,
              empty: false
            })
          }
        }
        that.setData({
          loadShow: true
        })
      }
    })
  },
  //单选择
  checkboxChange: function (e) {
    let that = this;
    let index = e.currentTarget.dataset.index;
    let checkboxItems = that.data.partsList;
    //   arr = e.detail.value;
    let len1 = checkboxItems.length,
      //   len2 = arr.length,
      len3 = 0;
    for (var i = 0; i < len1; i++) {
      //checkboxItems[i].checked = false;
      if (i == index) {
        checkboxItems[i].checked = !checkboxItems[i].checked;
      }
      if (checkboxItems[i].checked == true) {
        len3++;
      }
    }
    if (len3 == len1) {
      var checkedAll = true;
    } else {
      var checkedAll = false;
    }
    that.setData({
      partsList: checkboxItems,
      checkedAll: checkedAll
    })
  },
  //全选
  checkChange: function () {
    let that = this;
    let checkboxItems = that.data.partsList;
    let len = checkboxItems.length;
    let checkedAll = !that.data.checkedAll;
    for (var i = 0; i < len; i++) {
      checkboxItems[i].checked = checkedAll;
    }
    that.setData({
      checkedAll: checkedAll,
      partsList: checkboxItems
    })
  },
  //数量加减
  changeNUm: function (e) {
    var that = this;
    let type = e.currentTarget.dataset.type;
    let index = e.currentTarget.dataset.index;
    let num = that.data.partsList[index].goods_num;
    if (type == 0) {    //减
      num--;
    } else {          //加
      num++;
    }
    if (num < 1) {
      num = 1;
    }
    let goods_num = 'partsList[' + index + '].goods_num'
    that.setData({
      [goods_num]: num
    })
  },
  //输入搜索条件
  inputSearch: function (e) {
    var that = this;
    var key = e.detail.value;
    that.setData({
      search_name: key
    })
  },
  //小键盘条件搜索
  searchName: function (e) {
    var that = this;
    var searchName = e.detail.value;
    if (searchName == '' || searchName == null) {
      app.showToast("请输入配件名称", "none", 2000, function () { });
      return;
    }
    that.setData({
      search_name: searchName,
    })
    _PAGE = 1;
    that.getPartsList(searchName);
  },
  //点击图标搜索
  searchName2: function () {
    var that = this;
    var searchName = that.data.search_name;
    if (searchName == '' || searchName == null) {
      app.showToast("请输入配件名称", "none", 2000, function () { });
      return;
    }
    _PAGE = 1;
    that.getPartsList(searchName);
  },
  //跳转到选择配件
  newAdd: function () {
    var that = this;
    var partsList = that.data.partsList;
    var armatureList = [];
    var num = 0;
    for (var i in partsList) {
      if (partsList[i].checked == true) {
        armatureList[num] = partsList[i];
        num++;
      }
    }
    // if (armatureList == null || armatureList == '') {
    //   app.showToast("请选择配件", "none", 2000, function () { });
    //   return;
    // } else {
      wx.setStorageSync('armature', JSON.stringify(armatureList));
      wx.navigateBack({
        delta: 1
      })
    //}

  }
})