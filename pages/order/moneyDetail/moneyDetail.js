// pages/order/moneyDetail/moneyDetail.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    work_order_id:'',
    moneyDetail:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var work_order_id = options.work_order_id;
    that.setData({
      work_order_id: work_order_id
    });
    that.getMoneyDetail();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  // 获取费用详情
  getMoneyDetail:function(){
    var that = this;
    var work_order_id = that.data.work_order_id;
    app.ajax({
      url: 'Engineer/Workorder/getWorkCost',
      data:{
        work_order_id: work_order_id
      },
      success: function(res){
        if(res.data.code == 1000){
          that.setData({
            moneyDetail: res.data.data
          })
        }
      }
    })
  },

})